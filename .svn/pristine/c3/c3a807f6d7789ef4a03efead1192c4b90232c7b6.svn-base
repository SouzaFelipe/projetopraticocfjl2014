/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.framework;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.sun.xml.wss.impl.misc.Base64;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author felipesouza
 */
public class Relatorio {

    private StringBuilder relatorio;
    private String tituloRel;
    private String cabecalhoRel = "";
    private int numeroPagina = 0;
    private HashMap<Integer, String> hHTML;

    public String obterParametros(Class classe, Parametros params) {
        Funcoes f = new Funcoes();
        HashMap<String, String> mapJSON = new HashMap<>();
        StringBuilder strJSON = new StringBuilder("");
        String titulo = "";
        String nomeRel = this.getClass().getSimpleName();
        try {
            Field[] parametros = classe.getDeclaredFields();
            for (Field param : parametros) {
                Annotation[] anotacoes = param.getDeclaredAnnotations();
                for (Annotation anotacao : anotacoes) {
                    if (anotacao instanceof ParametroRel) {
                        ParametroRel atributos = (ParametroRel) anotacao;
                        mapJSON.clear();
                        mapJSON.put("nome", param.getName().toUpperCase());
                        mapJSON.put("descricao", atributos.descricao());
                        mapJSON.put("tipo", atributos.tipo());
                        mapJSON.put("ordem", "" + atributos.ordem());
                        mapJSON.put("chave", atributos.chave());
                        mapJSON.put("largura", "" + atributos.largura());
                        mapJSON.put("maxlength", "" + atributos.maxlength());
                        mapJSON.put("opcoesJSON", atributos.opcoesJSON());
                        mapJSON.put("readonly", atributos.readonly());
                        mapJSON.put("required", atributos.required());
                        mapJSON.put("valor", atributos.valor());
                        mapJSON.put("tabelaOrigem", atributos.tabelaOrigem());
                        mapJSON.put("campoOrigem", atributos.campoOrigem());
                        mapJSON.put("campoRetorno", atributos.campoRetorno());
                        mapJSON.put("onblur", atributos.onblur());
                        mapJSON.put("onfocus", atributos.onfocus());
                        mapJSON.put("onkeydown", atributos.onkeydown());
                        mapJSON.put("onkeypress", atributos.onkeypress());
                        strJSON.append(f.HashMapToJSON(mapJSON)).append(",");
                    } else if (anotacao instanceof Classe) {
                        Classe atributos = (Classe) anotacao;
                        titulo = atributos.titulo();
                    }
                }
            }
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #080."+nomeRel+".\nOcorreu um erro na tentativa de obter os parâmetros do relatório!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
        return "\"parametros\":[" + strJSON.substring(0, strJSON.length() - 1) + "], \"acao\": 1, \"rel\": \"" + params.getParameter("rel") + "\", \"titulo\":\"" + titulo + "\"";
    }

    public String preparaSQL(Class classe, String SQL, Parametros params) {
        String nomeRel = this.getClass().getSimpleName();
        String nomeCampo, tipo, valor;
        String SQLTemp = SQL;
        try {
            Field[] campos = classe.getDeclaredFields();
            for (Field campo : campos) {
                Annotation[] anotacoes = campo.getDeclaredAnnotations();
                for (Annotation anotacao : anotacoes) {
                    if (anotacao instanceof ParametroRel) {
                        ParametroRel atributos = (ParametroRel) anotacao;
                        tipo = atributos.tipo().trim();
                        nomeCampo = campo.getName().toUpperCase();
                        switch (tipo) {
                            case "I":
                                valor = (params.getParameter(nomeCampo) != null && !params.getParameter(nomeCampo).trim().equals("")) ? params.getParameter(nomeCampo).replace(".", "") : "0";
                                break;
                            case "D":
                                if (params.getParameter(nomeCampo) != null && !params.getParameter(nomeCampo).trim().equals("")) {
                                    String[] data = params.getParameter(nomeCampo).split("/");
                                    String vTemp;
                                    if (data.length == 3) {
                                        vTemp = data[0] + "." + data[1] + "." + data[2];
                                    } else {
                                        vTemp = "";
                                    }
                                    valor = "'" + vTemp + "'";
                                } else {
                                    valor = "''";
                                }
                                break;
                            case "N":
                                valor = (params.getParameter(nomeCampo) != null && !params.getParameter(nomeCampo).trim().equals("")) ? params.getParameter(nomeCampo).replace(".", "").replace(",", ".") : "0";
                                break;
                            default:
                                valor = "'" + ((params.getParameter(nomeCampo) != null && !params.getParameter(nomeCampo).trim().equals("")) ? params.getParameter(nomeCampo) : "") + "'";
                                break;
                        }
                        SQLTemp = SQLTemp.replace(":" + nomeCampo, valor);
                    }
                }
            }
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #081."+nomeRel+".\nOcorreu um erro ao preparar a consulta para relatório!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
        return SQLTemp;
    }

    public ResultSet executaSQL(String SQL) {
        DAO dao = new DAO();
        return dao.executaConsulta(SQL);
    }

    public String geraPDF() {
        String nomeRel = this.getClass().getSimpleName();
        try {
            String html = this.relatorio.toString();
            //Gera arquivo relatório
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(nomeRel + ".pdf"));
            document.open();
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, new ByteArrayInputStream(html.getBytes()));
            document.close();
            //Carrega arquivo
            File file = new File(nomeRel + ".pdf");
            byte[] bytes = loadFile(file);
            String encoded = Base64.encode(bytes);
            return encoded.replace("\n", "");
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #082." + nomeRel + ".\nOcorreu um erro ao gerar a visualização do relatório!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    private static byte[] loadFile(File file) {
        try {
            InputStream is = new FileInputStream(file);
            long length = file.length();
            if (length > Integer.MAX_VALUE) {
                // File is too large
            }
            byte[] bytes = new byte[(int) length];
            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }
            if (offset < bytes.length) {
                Erro erro = new Erro().getInstance();
                if (!erro.ocorreuErro()) {
                    String msgErro = "erro:Código de erro #083.\nNão foi possível ler o arquivo completamente!";
                    erro.setErro(msgErro, "");
                }
                return null;
            }
            is.close();
            return bytes;
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #082.\nOcorreu um erro ao tentar carregar o arquivo do relatório!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public void iniciaRel() {
        this.hHTML = new HashMap<>();
        String nomeClasse = this.getClass().getSimpleName();
        try {
            Field[] parametros = this.getClass().getDeclaredFields();
            for (Field param : parametros) {
                Annotation[] anotacoes = param.getDeclaredAnnotations();
                for (Annotation anotacao : anotacoes) {
                    if (anotacao instanceof Classe) {
                        Classe atributos = (Classe) anotacao;
                        this.tituloRel = atributos.titulo();
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #080." + nomeClasse + ".\n"
                        + "Ocorreu um erro na tentativa de inicializar o relatório!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
        }
    }

    public void addHTMLRel(String html) {
        this.hHTML.put(this.hHTML.size(), html);
    }

    public void finalizaRel() {
        this.relatorio = new StringBuilder();
        this.relatorio.append("<html>");
        this.relatorio.append("    <head>");
        this.relatorio.append("        <title>").append(this.tituloRel).append("</title>");
        this.relatorio.append("        <style>");
        this.relatorio.append("            * { font-family: Verdana; }");
        this.relatorio.append("            table { border-collapse:collapse; width:100%; }");
        this.relatorio.append("            th { background: #F7F7F7; padding:8px 8px 4px 8px; font-weight: bold; text-align:center; border:1px solid gray; }");
        this.relatorio.append("            td { padding:8px 8px 4px 8px; }");
        this.relatorio.append("            .centro { text-align:center; }");
        this.relatorio.append("            .direita { text-align:right; }");
        this.relatorio.append("            .negrito { font-weight: bold; }");
        this.relatorio.append("            .borda { border:1px solid gray; }");
        this.relatorio.append("            .erro { text-align:center; padding:18px 10px 14px 10px; font-size:20px; font-weight: bold; color:red; }");
        this.relatorio.append("            .cabecalho, .cabecalho tr, .cabecalho td { margin: px 0px 15px 0px; }");
        this.relatorio.append("            .rodape, .rodape tr, .rodape td { margin: 15px 0px 0px 0px; }");
        this.relatorio.append("            .fonte-grande { font-size:28px; }");
        this.relatorio.append("            .fonte-media { font-size:14px; }");
        this.relatorio.append("            .fonte-pequena { font-size:9px; }");
        this.relatorio.append("        </style>");
        this.relatorio.append("    </head>");
        this.relatorio.append("    <body>");
        int qtde = this.hHTML.size(), i, contador = 1;
        for (i = 1; i <= qtde; i++) {
            if (contador > 30 || i == 1) {
                this.relatorio.append("<table class=\"cabecalho\">");
                this.relatorio.append("    <tr>");
                this.relatorio.append("        <td class=\"direita fonte-media\">").append(new Date().toLocaleString().substring(0, 16)).append("</td>");
                this.relatorio.append("    </tr>");
                this.relatorio.append("    <tr>");
                this.relatorio.append("        <td class=\"centro negrito fonte-grande\" style=\"padding-top: 10px;\">").append(this.tituloRel.toUpperCase()).append("</td>");
                this.relatorio.append("    </tr>");
                this.relatorio.append("</table>");
                this.relatorio.append("<table class=\"borda\">");
                this.relatorio.append(this.cabecalhoRel);
                contador = 1;
            }
            this.relatorio.append(hHTML.get(i));
            contador++;
            if (contador > 30) {
                this.relatorio.append("</table>");
                this.numeroPagina++;
                this.relatorio.append("<table class=\"rodape\">");
                this.relatorio.append("    <tr>");
                this.relatorio.append("        <td class=\"negrito fonte-pequena\">Copyright © 2013 - 2014 Grupo CI. Todos os direitos reservados.</td>");
                this.relatorio.append("        <td class=\"direita negrito fonte-media\" style=\"width: 100px;\">").append(this.numeroPagina).append("</td>");
                this.relatorio.append("    </tr>");
                this.relatorio.append("</table>");
            }
        }
        if (contador <= 30) {
            this.relatorio.append("</table>");
            this.numeroPagina++;
            this.relatorio.append("<table class=\"rodape\">");
            this.relatorio.append("    <tr>");
            this.relatorio.append("        <td class=\"negrito fonte-pequena\">Copyright © 2013 - 2014 Grupo CI. Todos os direitos reservados.</td>");
            this.relatorio.append("        <td class=\"direita negrito fonte-media\" style=\"width: 100px;\">").append(this.numeroPagina).append("</td>");
            this.relatorio.append("    </tr>");
            this.relatorio.append("</table>");
        }
        this.relatorio.append("    </body>");
        this.relatorio.append("</html>");
    }

    public void retornaErro() {
        this.relatorio.append("<tr><td class=\"erro\">Não foram localizados registros para a parametrização utilizada!</td></tr>");
    }

    public void addCabecalhoTabela(String html) {
        this.cabecalhoRel = html;
    }
}
