var camposFalse = [];
var primeiro = "";

function validaObrigatorio(campos) {
    for (var i = 0; i < campos.length; i++) {
        if (campos[i].querySelector("input, select").required === true && campos[i].querySelector("input, select").value === "" && campos[i].querySelector("input, select").readOnly === false) {
            campos[i].querySelector("input, select").style.borderColor = "red";
            camposFalse.push(campos[i].querySelector("label").textContent);
            if (primeiro === "") {
                primeiro = campos[i].querySelector("input, select");
            }
        } else {
            campos[i].querySelector("input, select").style.borderColor = "";
        }
    }
    if (camposFalse.length > 0) {
        return false;
    } else {
        return true;
    }
}

function validaInteiro(campos) {
    for (var i = 0; i < campos.length; i++) {
        if (campos[i].querySelector("input, select").readOnly === false && campos[i].querySelector("input").getAttribute("tipo") === "I" && (isNaN(campos[i].querySelector("input").value) || campos[i].querySelector("input").value.indexOf(",") > -1 || campos[i].querySelector("input").value.indexOf(".") > -1)) {
            campos[i].querySelector("input").style.borderColor = "red";
            camposFalse.push(campos[i].querySelector("label").textContent);
            if (primeiro === "") {
                primeiro = campos[i].querySelector("input");
            }
        } else {
            campos[i].querySelector("input, select").style.borderColor = "";
        }
    }
    if (camposFalse.length > 0) {
        return false;
    } else {
        return true;
    }
}

function validaCPFCNPJ(campos) {
    for (var k = 0; k < campos.length; k++) {
        if (campos[k].querySelector("input, select").readOnly === false && campos[k].querySelector("input").getAttribute("tipo") === "CPFCNPJ") {
            if (campos[k].querySelector("input").value.length === 14) {
                var soma = 0;
                var resto;
                var strCPF = campos[k].querySelector("input, select").value;
                strCPF = strCPF.replace(".", "");
                strCPF = strCPF.replace(".", "");
                strCPF = strCPF.replace("-", "");
                for (var i = 1; i <= 9; i++) {
                    soma += parseInt(strCPF.substring(i - 1, i)) * (11 - i);
                }
                resto = eval((soma * 10) % 11);
                if ((resto === 10) || (resto === 11)) {
                    resto = 0;
                } else if (resto !== parseInt(strCPF.substring(9, 10))) {
                    campos[k].querySelector("input").style.borderColor = "red";
                    camposFalse.push(campos[k].querySelector("label").textContent);
                    if (primeiro === "") {
                        primeiro = campos[k].querySelector("input");
                    }
                }
                soma = 0;
                for (var i = 1; i <= 10; i++) {
                    soma += parseInt(strCPF.substring(i - 1, i)) * (12 - i);
                }
                resto = (soma * 10) % 11;
                if ((resto === 10) || (resto === 11))
                    resto = 0;
                if (resto !== parseInt(strCPF.substring(10, 11))) {
                    campos[k].querySelector("input").style.borderColor = "red";
                    if (camposFalse.indexOf(campos[k].querySelector("label").textContent) > -1) {
                    } else {
                        camposFalse.push(campos[k].querySelector("label").textContent);
                    }
                    if (primeiro === "") {
                        primeiro = campos[k].querySelector("input");
                    }
                }
            } else if (campos[k].querySelector("input").value.length === 18) {
                strCNPJ = campos[k].querySelector("input").value;
                strCNPJ = strCNPJ.replace(".", "");
                strCNPJ = strCNPJ.replace(".", "");
                strCNPJ = strCNPJ.replace("/", "");
                strCNPJ = strCNPJ.replace("-", "");
                var ini, j, i, soma, pos;
                ini = 5;
                pos = 2;
                j = 0;
                soma = 0;
                for (i = 1; i <= strCNPJ.length - 2; i++) {
                    if ((ini + i) <= 9) {
                        soma += parseInt(strCNPJ.substring(i - 1, i)) * (ini + i);
                    } else {
                        soma += parseInt(strCNPJ.substring(i - 1, i)) * (pos + j);
                        j++;
                    }
                }
                dv1 = soma % 11;
                if (dv1 !== parseInt(strCNPJ.substring(12, 13))) {
                    campos[k].querySelector("input").style.borderColor = "red";
                    camposFalse.push(campos[k].querySelector("label").textContent);
                    if (primeiro === "") {
                        primeiro = campos[k].querySelector("input");
                    }
                }
                ini = 4;
                pos = 2;
                j = 0;
                soma = 0;
                for (i = 1; i <= strCNPJ.length - 1; i++) {
                    if ((ini + i) <= 9) {
                        soma += parseInt(strCNPJ.substring(i - 1, i)) * (ini + i);
                    } else {
                        soma += parseInt(strCNPJ.substring(i - 1, i)) * (pos + j);
                        j++;
                    }
                }
                dv2 = soma % 11;
                if (dv2 !== parseInt(strCNPJ.substring(13, 14))) {
                    campos[k].querySelector("input").style.borderColor = "red";
                    if (camposFalse.indexOf(campos[k].querySelector("label").textContent) > -1) {
                    } else {
                        camposFalse.push(campos[k].querySelector("label").textContent);
                    }
                    if (primeiro === "") {
                        primeiro = campos[k].querySelector("input");
                    }
                }
            } else {
                campos[k].querySelector("input").style.borderColor = "red";
                camposFalse.push(campos[k].querySelector("label").textContent);
                if (primeiro === "") {
                    primeiro = campos[k].querySelector("input");
                }
            }
        } else {
            campos[k].querySelector("input, select").style.borderColor = "";
        }
    }
    if (camposFalse.length > 0) {
        return false;
    } else {
        return true;
    }
}

function validaData(campos) {
    var data;
    var regex = new RegExp("(0[1-9]|[1][0-9]|2[0-8])\/(0[1-9]|1[0-2])\/[0-9]{4}|(29|30)\/(0[13456789]|1[0-2])\/[0-9]{4}|(31)\/(0[13578]|1[02])\/[0-9]{4}|(29)\/(02)\/(([0-9]{2})(0[48]|[2468][048]|[13579][26])|(20)?00)");
    for (var i = 0; i < campos.length; i++) {
        if (campos[i].querySelector("input, select").readOnly === false && campos[i].querySelector("input").getAttribute("TIPO") === "D" && campos[i].querySelector("input").value !== "") {
            data = campos[i].querySelector("input").value;
            if (regex.exec(data)) {
                campos[i].querySelector("input, select").style.borderColor = "";
            } else {
                campos[i].querySelector("input").style.borderColor = "red";
                camposFalse.push(campos[i].querySelector("label").textContent);
                if (primeiro === "") {
                    primeiro = campos[i].querySelector("input");
                }
            }
        }
    }
    if (camposFalse.length > 0) {
        return false;
    } else {
        return true;
    }
}


function validaCampos(frm) {
    camposFalse = [];
    primeiro = "";
    var campos = frm.querySelectorAll(".div-campo");
    var c;
    if (validaObrigatorio(campos) === false) {
        c = camposFalse.join("\n");
        window.alert("Os seguintes campos não podem estar vazios:\n" + c);
        primeiro.focus();
    } else if (validaInteiro(campos) === false) {
        c = camposFalse.join("\n");
        window.alert("Os seguintes campos devem ser números inteiros:\n" + c);
        primeiro.focus();
    } else if (validaCPFCNPJ(campos) === false) {
        c = camposFalse.join("\n");
        window.alert("Verifique os seguintes campos:\n" + c);
        primeiro.focus();
    } else if (validaData(campos) === false) {
        c = camposFalse.join("\n");
        window.alert("A data informada não é uma data válida. Verifique os seguintes campos:\n" + c);
        primeiro.focus();
    } else {
        return true;
    }
}