/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.framework;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;

/**
 *
 * @author felipesouza
 */
public class Processo {
    
    public String obterTela(Parametros parametros) {
        try {
            HashMap<String, String> mapJSON = new HashMap<>();
            StringBuilder strJSON = new StringBuilder("");
            StringBuilder btnJSON = new StringBuilder("");
            StringBuilder tituloJSON = new StringBuilder("");
            StringBuilder retJSON = new StringBuilder("");
            String vCampo, titulo = "";
            Field[] campos = this.getClass().getDeclaredFields();
            Funcoes f = new Funcoes();
            for (Field campo : campos) {
                Annotation[] anotacoes = campo.getDeclaredAnnotations();
                for (Annotation anotacao : anotacoes) {
                    if (anotacao instanceof Campo) {
                        Campo atributos = (Campo) anotacao;
                        //Seta os atributos
                        mapJSON.clear();
                        mapJSON.put("nome", campo.getName().toUpperCase());
                        mapJSON.put("descricao", atributos.descricao());
                        mapJSON.put("tipo", atributos.tipo());
                        mapJSON.put("chave", atributos.chave());
                        if (atributos.chave().equals("F")) {
                            mapJSON.put("valorRetorno", parametros.getParameter(atributos.campoRetorno()));
                            parametros.setParameter(atributos.campoRetorno(),"");
                        }
                        if (atributos.tipo().equals("CD")) {
                            DAO dao = new DAO();
                            mapJSON.put("opcoes", dao.executaConsulta(atributos.tabelaOrigem(), atributos.campoOrigem(), atributos.campoRetorno()));
                        } else {
                            mapJSON.put("opcoes", atributos.opcoesJSON());
                        }
                        vCampo = parametros.getParameter(campo.getName().toUpperCase());
                        if (atributos.tipo().equals("D")) {
                            mapJSON.put("valor", (vCampo != null && !vCampo.trim().equals("")) ? vCampo.replace("-", "/") : atributos.valor());
                        } else {
                            mapJSON.put("valor", (vCampo != null && !vCampo.trim().equals("")) ? vCampo : atributos.valor());
                        }
//                        mapJSON.put("valor", atributos.valor());
                        mapJSON.put("required", atributos.required());
                        mapJSON.put("largura", "" + atributos.largura());
                        mapJSON.put("maxlength", "" + atributos.maxlength());
                        mapJSON.put("readonly", atributos.readonly());
                        mapJSON.put("visivel", atributos.visivel());
                        mapJSON.put("grid", atributos.grid());
                        mapJSON.put("ordem", "" + atributos.ordem());
                        mapJSON.put("tabelaOrigem", "" + atributos.tabelaOrigem());
                        mapJSON.put("campoOrigem", "" + atributos.campoOrigem());
                        mapJSON.put("campoRetorno", "" + atributos.campoRetorno());
                        mapJSON.put("labelCampoRetorno", "" + atributos.labelCampoRetorno());

                        //Seta os eventos
                        mapJSON.put("onfocus", atributos.onfocus());
                        mapJSON.put("onkeydown", atributos.onkeydown());
                        mapJSON.put("onkeypress", atributos.onkeypress());
                        mapJSON.put("onblur", atributos.onblur());
                        strJSON.append(f.HashMapToJSON(mapJSON)).append(",");
                    } else if (anotacao instanceof Botao) {
                        Botao botao = (Botao) anotacao;
                        mapJSON.clear();
                        mapJSON.put("id", campo.getName().toUpperCase());
                        mapJSON.put("label", botao.label());
                        mapJSON.put("ordem", "" + botao.ordem());
                        mapJSON.put("onclick", botao.onclick());
                        btnJSON.append(f.HashMapToJSON(mapJSON)).append(",");
                    } else if (anotacao instanceof Classe) {
                        if (titulo.trim().equals("")) {
                            Classe classe = (Classe) anotacao;
                            mapJSON.clear();
                            titulo = classe.titulo();
//                            mapJSON.put("titulo", classe.titulo());
//                            tituloJSON.append(f.HashMapToJSON(mapJSON));
                        }
                    }
                }
            }
            retJSON.append("\"titulo\":\"").append(titulo)
                    .append("\", \"campos\":[").append(strJSON.substring(0, strJSON.length() - 1))
                    .append("], \"botoes\":[").append(btnJSON.substring(0, btnJSON.length() - 1)).append("]");
            return retJSON.toString();
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #050.\nOcorreu um erro na tentativa de obter a tela de processo!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }
    
}
