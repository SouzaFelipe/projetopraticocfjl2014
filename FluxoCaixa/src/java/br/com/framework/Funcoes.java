package br.com.framework;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Felipe
 */
public class Funcoes {

    public String HashMapToJSON(HashMap<String, String> hash) {
        try {
            StringBuilder strJSON = new StringBuilder();
            StringBuilder retJSON = new StringBuilder();
            for (Map.Entry<String, String> entry : hash.entrySet()) {
                if (!entry.getKey().equals("opcoes")) {
                    strJSON.append("\"")
                            .append(entry.getKey())
                            .append("\":\"")
                            .append(entry.getValue())
                            .append("\",");
                } else {
                    strJSON.append("\"")
                            .append(entry.getKey())
                            .append("\":")
                            .append(entry.getValue())
                            .append(",");
                }
            }
            retJSON.append("{").append(strJSON.substring(0, strJSON.length() - 1)).append("}");
            return retJSON.toString();
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #040.\nOcorreu erro na tentativa de executar HashMapToJSON!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public String HashMapToSQL(String tabela, String tipo, String campoChave, String codigo, HashMap<String, String> hash, HashMap<String, String> tipos) {
        try {
            StringBuilder retSQL = new StringBuilder();
            StringBuilder tempSQL = new StringBuilder();
            StringBuilder tempSQL2 = new StringBuilder();
            if (tipo.equals("I")) {
                //Monta Insert
                tempSQL.append("INSERT INTO ").append(tabela).append(" (");
                tempSQL2.append(" VALUES (");
                for (Map.Entry<String, String> entry : hash.entrySet()) {
                    tempSQL.append(entry.getKey().toUpperCase());
                    if (tipos.get(entry.getKey()).equals("int") || tipos.get(entry.getKey()).equals("double")) {
                        tempSQL2.append(entry.getValue());
                    } else {
                        tempSQL2.append("'").append(entry.getValue()).append("'");
                    }
                    tempSQL.append(", ");
                    tempSQL2.append(", ");
                }
                retSQL.append(tempSQL.substring(0, tempSQL.length() - 2))
                        .append(")")
                        .append(tempSQL2.substring(0, tempSQL2.length() - 2))
                        .append(")");
            } else {
                //Monta Update
                tempSQL.append("UPDATE ")
                        .append(tabela)
                        .append(" SET ");
                for (Map.Entry<String, String> entry : hash.entrySet()) {
                    if (!entry.getKey().toUpperCase().equals(campoChave)) {
                        tempSQL.append(entry.getKey().toUpperCase())
                                .append(" = ");
                        if (tipos.get(entry.getKey()).equals("int") || tipos.get(entry.getKey()).equals("double")) {
                            tempSQL.append(entry.getValue());
                        } else {
                            tempSQL.append("'").append(entry.getValue()).append("'");
                        }
                        tempSQL.append(", ");
                    }
                }
                retSQL.append(tempSQL.substring(0, tempSQL.length() - 2))
                        .append(" WHERE ")
                        .append(tabela)
                        .append(".")
                        .append(campoChave)
                        .append(" = ")
                        .append(codigo);
            }
            return retSQL.toString();
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #041.\nOcorreu erro na tentativa de executar HashMapToSQL!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public Parametros preparaParametros(HttpServletRequest request) throws IOException {
        Parametros parametros = new Parametros();
        Map<String, String[]> a = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : a.entrySet()) {
            parametros.setParameter(entry.getKey(), new String(entry.getValue()[0].getBytes("iso-8859-1"), "UTF-8"));
        }
        return parametros;
    }

    public String getSQLInsertUpdate(Object objClas, int codigo, String tipo) {
        try {
            StringBuilder retSQL = new StringBuilder();
            HashMap<String, String> hashCampos = new HashMap<>();
            HashMap<String, String> hashTipos = new HashMap<>();
            Class classAtual = objClas.getClass();
            String nomeTabela = classAtual.getSimpleName().toUpperCase();
            Field[] campos = classAtual.getDeclaredFields();
            boolean requerido = false;
            String cChave = "";
            for (Field campo : campos) {
                Annotation[] anotacoes = campo.getDeclaredAnnotations();
                for (Annotation anotacao : anotacoes) {
                    if (anotacao instanceof Campo) {
                        Campo atributos = (Campo) anotacao;
                        requerido = false;
                        if (atributos.required().equals("S")) {
                            requerido = true;
                        }
                        if (atributos.chave().equals("P")) {
                            cChave = campo.getName().toUpperCase();
                        }
                    }
                }
                campo.setAccessible(true);
                if (campo.get(objClas) != null && !campo.get(objClas).toString().equals("")) {
                    if (campo.getType().getSimpleName().equals("Date")) {
                        hashCampos.put(campo.getName(), ((Date) campo.get(objClas)).toLocaleString().substring(0, 10).replaceAll("/", "."));
                    } else {
                        hashCampos.put(campo.getName(), campo.get(objClas).toString());
                    }
                    hashTipos.put(campo.getName(), campo.getType().getSimpleName());
                } else if (requerido) {
                    //Verificar como fazer para esse erro subir ao client
                    Logger.getLogger(Tabela.class.getName()).log(Level.SEVERE, "ERRO VIOLENTO: Fuja para as colinas!!");
                }
            }
            retSQL.append(HashMapToSQL(nomeTabela, tipo, cChave, "" + codigo, hashCampos, hashTipos));
            return retSQL.toString();
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #042.\nOcorreu erro ao obter a SQL de inclusão/alteração!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public String getSQLDelete(Object objClas) {
        StringBuilder retSQL = new StringBuilder();
        Class classAtual = objClas.getClass();
        String nomeTabela = classAtual.getSimpleName().toUpperCase();
        Field[] campos = classAtual.getDeclaredFields();
        String cChave = "", vChave = "";
        try {
            for (Field campo : campos) {
                Annotation[] anotacoes = campo.getDeclaredAnnotations();
                for (Annotation anotacao : anotacoes) {
                    if (anotacao instanceof Campo) {
                        Campo atributos = (Campo) anotacao;
                        if (atributos.chave().equals("P")) {
                            campo.setAccessible(true);
                            cChave = campo.getName().toUpperCase();
                            vChave = "" + campo.get(objClas);
                            break;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #043.\nOcorreu erro ao obter a SQL de deleção!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
        retSQL.append("DELETE FROM ").append(nomeTabela).append(" WHERE ").append(nomeTabela).append(".").append(cChave).append(" = ").append(vChave);
        return retSQL.toString();
    }

    public Object executaMetodo(String classe, String metodo, Object[] parametros) {
        try {
            Class objClasse = Class.forName("br.com." + classe);
            Class[] tipoParams = new Class[parametros.length];
            for (int i = 0; i < parametros.length; i++) {
                Object superClasse = parametros[i].getClass().getSuperclass();
                Object nomeSuperClasse = parametros[i].getClass().getSuperclass().getSimpleName();
                if ((superClasse != null && nomeSuperClasse.equals("Tabela")) && !parametros[i].getClass().getSimpleName().equals("Parametros")) {
                    tipoParams[i] = Object.class;
                } else {
                    tipoParams[i] = parametros[i].getClass();
                }
            }
            Method objMetodo;
            try {
                objMetodo = objClasse.getMethod(metodo, tipoParams);
            } catch (NoSuchMethodException | SecurityException ex) {
                objMetodo = objClasse.getSuperclass().getMethod(metodo, tipoParams);
            }
            Constructor constructClasse = objClasse.getConstructor();
            Object objetoContruct = constructClasse.newInstance();
            Object objRetorno = objMetodo.invoke(objetoContruct, parametros);
            return objRetorno;
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #044.\nOcorreu um erro ao tentar executar o metodo \""+metodo+"\"!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public Object executaMetodo(Object classe, String metodo, Object[] parametros) {
        try {
            Class objClasse = classe.getClass();
            Class[] tipoParams = new Class[parametros.length];
            for (int i = 0; i < parametros.length; i++) {
                Object superClasse = parametros[i].getClass().getSuperclass();
                Object nomeSuperClasse = parametros[i].getClass().getSuperclass().getSimpleName();
                if ((superClasse != null && nomeSuperClasse.equals("Tabela")) && !parametros[i].getClass().getSimpleName().equals("Parametros")) {
                    tipoParams[i] = Object.class;
                } else {
                    tipoParams[i] = parametros[i].getClass();
                }
            }
            Method objMetodo;
            try {
                objMetodo = objClasse.getMethod(metodo, tipoParams);
            } catch (NoSuchMethodException | SecurityException ex) {
                objMetodo = objClasse.getSuperclass().getMethod(metodo, tipoParams);
            }
            Object objRetorno = objMetodo.invoke(classe, parametros);
            return objRetorno;
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #045.\nOcorreu um erro ao tentar executar o metodo \""+metodo+"\"!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

}
