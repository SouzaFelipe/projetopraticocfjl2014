package br.com.framework;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Felipe
 */
public class Tabela {

    public String obterTela() {
        try {
            HashMap<String, String> mapJSON = new HashMap<>();
            StringBuilder strJSON = new StringBuilder();
            StringBuilder opcJSON = new StringBuilder();
            StringBuilder retJSON = new StringBuilder();
            int nOrdem = -1;
            Field[] campos = this.getClass().getDeclaredFields();
            Funcoes f = new Funcoes();
            for (Field campo : campos) {
                Annotation[] anotacoes = campo.getDeclaredAnnotations();
                for (Annotation anotacao : anotacoes) {
                    if (anotacao instanceof Campo) {
                        Campo atributos = (Campo) anotacao;
                        //Seta os atributos
                        mapJSON.clear();
                        mapJSON.put("nome", campo.getName().toUpperCase());
                        mapJSON.put("descricao", atributos.descricao());
                        mapJSON.put("tipo", atributos.tipo());
                        mapJSON.put("chave", atributos.chave());
                        if (atributos.tipo().equals("CD")) {
                            DAO dao = new DAO();
                            mapJSON.put("opcoes", dao.executaConsulta(atributos.tabelaOrigem(), atributos.campoOrigem(), atributos.campoRetorno()));
                        } else {
                            mapJSON.put("opcoes", atributos.opcoesJSON());
                        }
                        mapJSON.put("valor", atributos.valor());
                        mapJSON.put("required", atributos.required());
                        mapJSON.put("largura", "" + atributos.largura());
                        mapJSON.put("maxlength", "" + atributos.maxlength());
                        mapJSON.put("readonly", atributos.readonly());
                        mapJSON.put("visivel", atributos.visivel());
                        mapJSON.put("grid", atributos.grid());
                        mapJSON.put("ordem", "" + atributos.ordem());
                        mapJSON.put("tabelaOrigem", "" + atributos.tabelaOrigem());
                        mapJSON.put("campoOrigem", "" + atributos.campoOrigem());
                        mapJSON.put("campoRetorno", "" + atributos.campoRetorno());
                        mapJSON.put("labelCampoRetorno", "" + atributos.labelCampoRetorno());
                        
                        //Seta os eventos
                        mapJSON.put("onfocus", atributos.onfocus());
                        mapJSON.put("onkeydown", atributos.onkeydown());
                        mapJSON.put("onkeypress", atributos.onkeypress());
                        mapJSON.put("onblur", atributos.onblur());
                        strJSON.append(f.HashMapToJSON(mapJSON)).append(",");
                    } else if (anotacao instanceof Opcoes) {
                        Opcoes opcoes = (Opcoes) anotacao;
                        //Seta os atributos
                        mapJSON.clear();
                        mapJSON.put("descricao", opcoes.descricao());
                        mapJSON.put("ordem", "" + opcoes.ordem());
                        if (opcoes.ordem() > nOrdem) {
                            nOrdem = opcoes.ordem();
                        }
                        //Seta os eventos
                        mapJSON.put("onclick", opcoes.onclick());
                        opcJSON.append(f.HashMapToJSON(mapJSON)).append(",");
                    }
                }
            }
            opcJSON.append("{\"descricao\": \"Exportar arquivo CSV\", \"ordem\": \"").append(nOrdem + 1).append("\", \"onclick\": \"obterDadosCSV\"},")
                    .append("{\"descricao\": \"Importar arquivo CSV\", \"ordem\": \"").append(nOrdem + 2).append("\", \"onclick\": \"importarArqCSV\"},");
            retJSON.append("\"campos\":[").append(strJSON.substring(0, strJSON.length() - 1)).append("], \"opcoes\":[").append(opcJSON.substring(0, opcJSON.length() - 1)).append("]");
            return retJSON.toString();
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #020.\nOcorreu um erro na tentativa de obter a tela!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public HashMap<String, Object[]> obterCamposConsulta() {
        try {
            HashMap<String, Object[]> hashCampos = new HashMap<>();
            Field[] campos = this.getClass().getDeclaredFields();
            Funcoes f = new Funcoes();
            Object[] dados;
            int count = 0;
            for (Field campo : campos) {
                Annotation[] anotacoes = campo.getDeclaredAnnotations();
                for (Annotation anotacao : anotacoes) {
                    if (anotacao instanceof Campo) {
                        Campo atributos = (Campo) anotacao;
                        //if (atributos.grid().equals("S")) {
                        //remover a condições e inserir  atributos.grid()
                        String nomeCampo = campo.getName().toUpperCase();
                        dados = new Object[4];
                        dados[0] = atributos.descricao();
                        dados[1] = atributos.ordem() + count;
                        dados[2] = atributos.tipo();
                        dados[3] = atributos.grid();
                        hashCampos.put(nomeCampo, dados);
                        
                        if (atributos.chave().equals("F")) {
                            count++;
                            nomeCampo = atributos.campoRetorno().toUpperCase();
                            dados = new Object[4];
                            dados[0] = atributos.labelCampoRetorno();
                            dados[1] = atributos.ordem() + count;
                            dados[2] = "S";
                            dados[3] = atributos.grid();
                            hashCampos.put(nomeCampo, dados);
                            count++;
                        }
                        //}   
                    }
                }
            }
            return hashCampos;
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #021.\nOcorreu um erro na tentativa de obter os campos da grid!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public String obterCamposTelaConsulta() {
        try {
            StringBuilder strCampos = new StringBuilder();
            StringBuilder retorno = new StringBuilder();
            Field[] campos = this.getClass().getDeclaredFields();
            for (Field campo : campos) {
                Annotation[] anotacoes = campo.getDeclaredAnnotations();
                for (Annotation anotacao : anotacoes) {
                    if (anotacao instanceof Campo) {
                        Campo atributos = (Campo) anotacao;
                        if (atributos.grid().equals("S")) {
                            String nomeCampo = campo.getName().toUpperCase();
                            String tipoCampo = campo.getType().getSimpleName();
                            strCampos.append("{\"id\":\"").append(nomeCampo).append("\", \"label\":\"").append(atributos.descricao())
                                    .append("\", \"tipo\":\"").append(tipoCampo).append("\"},");
//                                .append("\", \"tipo\":\"").append(atributos.tipo()).append("\"},");
                        }
                    }
                }
            }
            retorno.append("\"campos\":[").append(strCampos.substring(0, strCampos.length() - 1)).append("]");
            return retorno.toString();
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #022.\nOcorreu um erro ao obter os campos da tela de consulta!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public Integer preencheCampos(Parametros parametros) {
        try {
            SimpleDateFormat fData = new SimpleDateFormat("dd-MM-yyyy");
            String nomeCampo;
            boolean chave = false;
            Integer codigo = null;
            Field[] campos = this.getClass().getDeclaredFields();
            for (Field campo : campos) {
                nomeCampo = campo.getName().toUpperCase();
                Annotation[] anotacoes = campo.getDeclaredAnnotations();
                for (Annotation anotacao : anotacoes) {
                    if (anotacao instanceof Campo) {
                        Campo atributos = (Campo) anotacao;
                        if (atributos.chave().equals("P")) {
                            chave = true;
                        }
                        for (Map.Entry<String, String> param : parametros.getParameterMap().entrySet()) {
                            if (param.getKey().equals(nomeCampo)) {
                                campo.setAccessible(true);
                                Object valor;
                                if (campo.getType().getName().equals("int")) {
                                    valor = 0;
                                    if (!param.getValue().trim().equals("")) {
                                        valor = Integer.parseInt(param.getValue());
                                    }
                                } else if (campo.getType().getSimpleName().equals("Date")) {
                                    if (param.getValue().indexOf("/")>-1){
                                        valor = !param.getValue().trim().equals("") ? fData.parse(param.getValue().replaceAll("/", "-")) : null;
                                    } else {
                                        valor = !param.getValue().trim().equals("") ? fData.parse(param.getValue()) : null;
                                    }
                                } else if (campo.getType().getSimpleName().equals("double")) {
                                    valor = !param.getValue().trim().equals("") ? new Double(param.getValue().replace(".", "").replace(",", ".")) : 0;
                                } else {
                                    valor = param.getValue();
                                }
                                campo.set(this, valor);
                                if (chave) {
                                    codigo = campo.getInt(this);
                                    chave = false;
                                }
                            }
                        }
                    }
                }
            }
            return codigo;
        } catch (Exception ex) {
            ex.printStackTrace();
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #023.\nOcorreu um erro na tentativa de preencher os campos da tabela!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }
}
