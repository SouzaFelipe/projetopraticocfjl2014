package br.com.framework;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Felipe
 */
public class DB {

    public static Connection criaConexao() {
        try {
            //Carrega o driver de conexão com o FireBird;
            Class.forName("org.firebirdsql.jdbc.FBDriver");
//        Connection conexao = DriverManager.getConnection("jdbc:firebirdsql:192.168.21.156/3050:C:/Users/felipesouza/Desktop/FLUXOCAIXA/FLUXOCAIXA.FDB", "sysdba", "masterkey");
            Connection conexao = DriverManager.getConnection("jdbc:firebirdsql:localhost/3050:D:/projetopraticocfjl2014/FLUXOCAIXA.FDB", "sysdba", "masterkey");
//            Connection conexao = DriverManager.getConnection("jdbc:firebirdsql:localhost/3050:C:/Users/felipesouza/Desktop/FLUXOCAIXA/FLUXOCAIXA.FDB", "sysdba", "masterkey");
            return conexao;
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            String msgErro = "erro:Código de erro #001.\nNão foi possível conectar-se ao banco de dados!";
            if (false) {
                msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
            }
            erro.setErro(msgErro, ex.getMessage());
            return null;
        }
    }
}
