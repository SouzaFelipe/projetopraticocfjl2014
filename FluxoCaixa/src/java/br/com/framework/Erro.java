/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.framework;

/**
 *
 * @author felipesouza
 */
public class Erro {

    private static Erro erro = null;
    private static DAO dao = null;
    private String msgErro = null;
    private boolean ocorreuErro = false;

    public synchronized Erro getInstance() {
        if (erro == null) {
            erro = new Erro();
            dao = new DAO();
        }
        return erro;
    }

    public synchronized void setErro(String msgErro, String infoTecnica) {
        this.msgErro = msgErro;
        this.ocorreuErro = true;
        if (!this.msgErro.contains("#001")) {
            this.dao.gravaErro(msgErro, infoTecnica);
        }
    }

    public synchronized String getErro() {
        String msg = this.msgErro;
        this.msgErro = null;
        this.ocorreuErro = false;
        return msg;
    }

    public synchronized boolean ocorreuErro() {
        return this.ocorreuErro;
    }

}
