
package br.com.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Felipe
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Opcoes {
    //Atributos
    String descricao();                 //=> Label da opção
    int ordem() default -1;             //=> Ordem das opções
    //Eventos
    String onclick();        //=> Evento de clique da opção
}
