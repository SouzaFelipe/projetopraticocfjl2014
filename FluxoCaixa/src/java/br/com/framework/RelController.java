/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.framework;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author felipesouza
 */
public class RelController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param parametros servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processaRequest(Parametros parametros, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder retorno = new StringBuilder();
        try (PrintWriter out = response.getWriter()) {
            try {
                String rel = parametros.getParameter("rel");
                if (rel != null && !rel.trim().equals("")) {
                    Class tipoParametros[] = new Class[1];
                    tipoParametros[0] = Parametros.class;
                    Class classe = Class.forName("br.com.relatorios." + rel);
                    Method metodo = classe.getMethod("geraRelatorio", tipoParametros);
                    Object params[] = new Object[1];
                    params[0] = parametros;
                    Constructor constructClasse = classe.getConstructor();
                    Object objClasse = constructClasse.newInstance();
                    Object retMetodo = metodo.invoke(objClasse, params);
                    retorno.append((String) retMetodo);
                } else {
                    retorno.append("erro:Relatório não localizado!");
                }
                Erro erro = new Erro().getInstance();
                if (erro.ocorreuErro()) {
                    out.println(erro.getErro());
                } else {
                    out.println(retorno.toString());
                }
            } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InstantiationException | 
                     NoSuchMethodException | SecurityException | InvocationTargetException ex) {
                Erro erro = new Erro().getInstance();
                if (erro.ocorreuErro()) {
                    out.println(erro.getErro());
                } else {
                    out.println("erro:Ocorreu um erro desconhecido!");
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Funcoes f = new Funcoes();
        processaRequest(f.preparaParametros(request), response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Funcoes f = new Funcoes();
        processaRequest(f.preparaParametros(request), response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
