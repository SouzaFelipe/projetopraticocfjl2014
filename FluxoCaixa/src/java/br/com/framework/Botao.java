
package br.com.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Felipe
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Botao {
    //Atributos
    String label();                 //=> Label do Botão
    int ordem() default -1;         //=> Ordem dos campos
    String onclick();               //=> Evento de click do botão
}
