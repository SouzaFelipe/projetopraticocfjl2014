
package br.com.framework;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Felipe
 */
public class Parametros {
    
    private HashMap<String, String> parametros;

    public Parametros() {
        this.parametros = new HashMap<>();
    }

    public String getParameter(String parametro) {
        return parametros.get(parametro);
    }
    
    public String getParameter(int parametro) {
        int i = 0;
        for (Map.Entry<String, String> entry : this.parametros.entrySet()) {
            if (i==parametro) {
                return entry.getValue();
            }
            i++;
        }
        return null;
    }
    
    public HashMap<String,String> getParameterMap() {
        return this.parametros;
    }
    
    public void setParameter(String parametro, String valor) {
        this.parametros.put(parametro, valor);
    }
    
    public int length() {
        return this.parametros.size();
    }
}
