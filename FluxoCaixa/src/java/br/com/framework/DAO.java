/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.framework;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author felipesouza
 */
public class DAO {

    public String retornaDados(Object classe, String condicao) {
        StringBuilder retorno = new StringBuilder();
        DecimalFormat fDecimal = new DecimalFormat("#,###,##0.00");
        Class classAtual = this.getClass();
        String nomeTabela = classAtual.getSimpleName().substring(0, classAtual.getSimpleName().length() - 3);
        try (Connection conexao = DB.criaConexao()) {
            Statement stm = conexao.createStatement();
            String TOP = "", WHERE = "", DESC = "";
            if (condicao != null && !condicao.trim().equals("")) {
                WHERE = " WHERE " + condicao;
            } else {
                TOP = "FIRST 100";
                DESC = " DESC";
            }
//            String SQL = "SELECT " + TOP + " * FROM " + nomeTabela.toUpperCase() + " T " + WHERE + " ORDER BY 1" + DESC;
            ////////////////////////////////////////////////////////////////////
            StringBuilder camposSQL = new StringBuilder(""), joinSQL = new StringBuilder("");
            int apelidoSQL = 1;
            String apelido = "";
            Class nClass = classe.getClass();
            Field[] campos = nClass.getDeclaredFields();
            campos:
            for (Field campo : campos) {
                Annotation[] anotacoes = campo.getDeclaredAnnotations();
                for (Annotation anotacao : anotacoes) {
                    if (anotacao instanceof Campo) {
                        Campo atributos = (Campo) anotacao;
                        camposSQL.append("T.").append(campo.getName().toUpperCase()).append(", ");
                        if (atributos.chave().equals("F")) {
                            apelido = " T"+apelidoSQL;
                            camposSQL.append(apelido).append(".").append(atributos.campoRetorno().toUpperCase()).append(", ");
                            joinSQL.append(" LEFT OUTER JOIN ").append(atributos.tabelaOrigem().toUpperCase()).append(apelido).append(" ON ")
                                    .append("(").append(apelido).append(".").append(atributos.campoOrigem()).append(" = T.").append(campo.getName().toUpperCase()).append(") ");
                            apelidoSQL++;
                        }
                    }
                }
            }
            String SQL = "SELECT " + TOP + " " + camposSQL.toString().substring(0, camposSQL.toString().length()-2) +
                    " FROM " + nomeTabela.toUpperCase() + " T " + joinSQL.toString() + WHERE + " ORDER BY 1" + DESC;
            System.out.println(SQL);

            ////////////////////////////////////////////////////////////////////
//            if (condicao != null && !condicao.trim().equals("")) {
//                SQL += " WHERE "+condicao;
//            }
//            SQL += " ORDER BY 1";
            ResultSet rs = stm.executeQuery(SQL);
            StringBuilder cabecalho = new StringBuilder();
            StringBuilder cabecalhoTmp = new StringBuilder();
            StringBuilder linhas = new StringBuilder();
            StringBuilder linhasTmp = new StringBuilder();

            Funcoes f = new Funcoes();
            HashMap<String, Object[]> hCampos = (HashMap<String, Object[]>) f.executaMetodo(classe, "obterCamposConsulta", new Object[0]);
            cabecalho.append("\"cabecalho\": [");
            for (Map.Entry<String, Object[]> entry : hCampos.entrySet()) {
                cabecalhoTmp.append("{\"campo\": \"").append(entry.getKey())
                        .append("\", \"label\": \"").append((String) entry.getValue()[0])
                        .append("\", \"grid\": \"").append((String) entry.getValue()[3])
                        //adiciona aki a grid
                        .append("\", \"ordem\": ").append((Integer) entry.getValue()[1]).append("},");
            }
            cabecalho.append(cabecalhoTmp.toString().substring(0, cabecalhoTmp.toString().length() - 1));
            cabecalho.append("],");

            while (rs.next()) {
                linhas.append("{\"colunas\":{");
                linhasTmp.setLength(0);
                for (Map.Entry<String, Object[]> entry : hCampos.entrySet()) {
                    if (entry.getValue()[2].equals("D") && rs.getDate(entry.getKey()) != null) {
                        linhasTmp.append("\"").append(entry.getKey()).append("\":\"").append(rs.getDate(entry.getKey()).toLocaleString().substring(0, 10)).append("\",");
                    } else if (entry.getValue()[2].equals("N") && rs.getString(entry.getKey()) != null) {
                        linhasTmp.append("\"").append(entry.getKey()).append("\":\"").append(fDecimal.format(rs.getDouble(entry.getKey()))).append("\",");
                    } else if (rs.getString(entry.getKey()) != null) {
                        linhasTmp.append("\"").append(entry.getKey()).append("\":\"").append(rs.getString(entry.getKey())).append("\",");
                    } else {
                        linhasTmp.append("\"").append(entry.getKey()).append("\":\"\",");
                    }
                }
                linhas.append(linhasTmp.toString().substring(0, linhasTmp.toString().length() - 1));
                linhas.append("}},");
            }
            String tabDados;
            if (linhas.toString().equals("")) {
                tabDados = "";
            } else {
                tabDados = "\"linhas\":[" + linhas.toString().substring(0, linhas.toString().length() - 1) + "]";
            }
            retorno.append(cabecalho.toString()).append("\"tabela\":{").append(tabDados).append("},")
                    .append("\"id\":\"").append(nomeTabela).append("\"");
            conexao.close();
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #010.\nOcorreu um erro ao buscar os registros!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
        return retorno.toString();
    }

    public String gravaDados(Object classe, Integer codigo) {
        Class classAtual = this.getClass();
        String nomeTabela = classAtual.getSimpleName().substring(0, classAtual.getSimpleName().length() - 3);
        try (Connection conexao = DB.criaConexao()) {
            Statement stm = conexao.createStatement();
            String acao = "A";
            if (codigo == null || codigo <= 0) {
                acao = "I";
                ResultSet rs = stm.executeQuery("SELECT NEXT VALUE FOR G_" + nomeTabela.toUpperCase() + " CODIGO FROM RDB$DATABASE");
                if (rs.next()) {
                    codigo = rs.getInt("CODIGO");
                }
            }
            if (codigo != null) {
                Class nClass = classe.getClass();
                Field[] campos = nClass.getDeclaredFields();
                try {
                    campos:
                    for (Field campo : campos) {
                        Annotation[] anotacoes = campo.getDeclaredAnnotations();
                        for (Annotation anotacao : anotacoes) {
                            if (anotacao instanceof Campo) {
                                Campo atributos = (Campo) anotacao;
                                if (atributos.chave().equals("P")) {
                                    String sql = "SELECT * FROM " + nomeTabela.toUpperCase() + " WHERE " + campo.getName().toUpperCase() + " = " + codigo;
                                    ResultSet rs = stm.executeQuery(sql);
                                    if (rs.next()) {
                                        campo.setAccessible(true);
                                        campo.set(classe, codigo);
                                    } else {
                                        acao = "I";
                                        rs = stm.executeQuery("SELECT NEXT VALUE FOR G_" + nomeTabela.toUpperCase() + " CODIGO FROM RDB$DATABASE");
                                        if (rs.next()) {
                                            codigo = rs.getInt("CODIGO");
                                        }
                                        campo.setAccessible(true);
                                        campo.set(classe, codigo);

                                    }
                                    break campos;
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    Erro erro = new Erro().getInstance();
                    if (!erro.ocorreuErro()) {
                        String msgErro = "erro:Código de erro #011.\nOcorreu um erro ao tentar gravar os dados!";
                        if (false) {
                            msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                        }
                        erro.setErro(msgErro, ex.getMessage());
                    }
                    return null;
                }
                Funcoes f = new Funcoes();
                String SQL = f.getSQLInsertUpdate(classe, codigo, acao);
                stm.executeUpdate(SQL);
            }
            conexao.close();
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #012.\nOcorreu um erro ao tentar gravar os dados!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
        return "\"codigo\":" + codigo;
    }

    public String deletaDados(Object classe) {
        try (Connection conexao = DB.criaConexao()) {
            Statement stm = conexao.createStatement();
            Funcoes f = new Funcoes();
            String SQL = f.getSQLDelete(classe);
            stm.executeUpdate(SQL);
            conexao.close();
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #013.\nOcorreu um erro ao tentar deletar o registro!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
        return "\"retorno\":\"OK\"";
    }

    public String executaConsulta(String tabela, String campoCodigo, String campoDescritivo) {
        try (Connection conexao = DB.criaConexao()) {
            Statement stm = conexao.createStatement();
            ResultSet rs = stm.executeQuery("SELECT " + campoCodigo + ", " + campoDescritivo + " FROM " + tabela);
            StringBuilder linhas = new StringBuilder();
            while (rs.next()) {
                linhas.append("{\"valor\":\"").append(rs.getString(campoCodigo)).append("\", \"label\":\"").append(rs.getString(campoDescritivo)).append("\"},");
            }
            conexao.close();
            return "[" + ((linhas.toString().length() > 0) ? linhas.toString().substring(0, linhas.toString().length() - 1) : "{}") + "]";
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #014.\nOcorreu um erro ao executar a consulta (ComboBox)!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public ArrayList consultaChaves(String tabela) {
        ArrayList chaves = new ArrayList();
        try {
            Connection conexao = DB.criaConexao();
            Statement stm = conexao.createStatement();
            String sql = "SELECT COD" + tabela.toUpperCase() + " FROM " + tabela.toUpperCase();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                chaves.add(rs.getString("COD" + tabela.toUpperCase()));
            }
            return chaves;
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #015.\nOcorreu um erro ao consultar as chaves no processo \"Importar arquivo csv\"!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public void gravaErro(String msgErro, String infoTecnica) {
        try (Connection conexao = DB.criaConexao()) {
            Statement stm = conexao.createStatement();
            ResultSet rs = stm.executeQuery("SELECT NEXT VALUE FOR G_TABERRO CODIGO FROM RDB$DATABASE");
            Integer codigo = null;
            if (rs.next()) {
                codigo = rs.getInt("CODIGO");
            }
            String data = new Date().toLocaleString().split(" ")[0].replace("/", ".");
            String hora = new Date().toLocaleString().split(" ")[1];
            String strErro = msgErro;
            if (!strErro.contains("Informações técnicas:") && !infoTecnica.trim().equals("")) {
                strErro += "\n\nInformações técnicas:\n" + infoTecnica;
            }
            String SQL = ("INSERT INTO TABERRO (CODTABERRO, ERRO, DATA, HORA, USUARIO) VALUES (" + codigo + ",'" + strErro.replace("erro:", "") + "','" + data + "','" + hora + "',NULL)").replace("\n", "'||ASCII_CHAR(13)||'");
            stm.executeUpdate(SQL);
            conexao.close();
        } catch (Exception ex) {
        }
    }

    public ResultSet executaConsulta(String SQL) {
        try {
            Connection conexao = DB.criaConexao();
            Statement stm = conexao.createStatement();
            return stm.executeQuery(SQL);
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #016.\nOcorreu um erro ao executar a consulta!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public String executaConsulta(String tabela, String campoCodigo, String campoDescritivo, String valorFk) {
        try {
            Connection conexao = DB.criaConexao();
            Statement stm = conexao.createStatement();
            ResultSet rs = stm.executeQuery("SELECT " + campoDescritivo + " FROM " + tabela + " WHERE " + campoCodigo + " = " + valorFk);
            StringBuilder linhas = new StringBuilder();
            if (rs.next()) {
                linhas.append("{\"RETORNO\":\"").append(rs.getString(campoDescritivo)).append("\"}");
            } else {
                linhas.append("{\"ERRO\":\"Registro não encontrado!\"}");
            }
            conexao.close();
            return linhas.toString();
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #017.\nOcorreu um erro ao executar a consulta!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

}
