/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.framework;

/**
 *
 * @author felipesouza
 */
public class View {

    public String getTela(Object classe) {
        try {
            String nomeTabela = this.getClass().getSimpleName().substring(0, this.getClass().getSimpleName().length() - 10);
            Funcoes f = new Funcoes();
            Object[] parametros;
            //Busca o JSON para desenhar a tela
            parametros = new Object[0];
            String partJSON1 = (String) f.executaMetodo(classe, "obterTela", parametros);
            //Busca JSON (grid) para atualizar a tela
            parametros = new Object[1];
            parametros[0] = classe;
            String partJSON2 = (String) f.executaMetodo("control." + nomeTabela + "DAO", "selecionaDados", parametros);
            return "{\"tela\":{" + partJSON1 + "," + partJSON2 + "}}";
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #030.\nOcorreu um erro na tentativa de obter a tela!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public String saveDados(Object classe, Parametros params) {
        try {
            String nomeTabela = this.getClass().getSimpleName().substring(0, this.getClass().getSimpleName().length() - 10);
            Funcoes f = new Funcoes();
            Object[] parametros;
            //Preenche os campos da classe Model
            parametros = new Object[1];
            parametros[0] = params;
            Integer codigo = (Integer) f.executaMetodo(classe, "preencheCampos", parametros);
            //Salva os dados em banco
            parametros = new Object[2];
            parametros[0] = classe;
            parametros[1] = codigo;
            String nCodigo = (String) f.executaMetodo("control." + nomeTabela + "DAO", "salvaDados", parametros);
            //Busca JSON (grid) para atualizar a tela
            parametros = new Object[1];
            parametros[0] = classe;
            String partJSON = (String) f.executaMetodo("control." + nomeTabela + "DAO", "selecionaDados", parametros);
            return "{\"tela\":{" + nCodigo + "," + partJSON + "}}";
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #031.\nOcorreu um erro na tentativa de salvar os dados!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public String deleteDados(Object classe, Parametros params) {
        try {
            String nomeTabela = this.getClass().getSimpleName().substring(0, this.getClass().getSimpleName().length() - 10);
            Funcoes f = new Funcoes();
            Object[] parametros;
            //Preenche os campos da classe Model
            parametros = new Object[1];
            parametros[0] = params;
            f.executaMetodo(classe, "preencheCampos", parametros);
            //Salva os dados em banco
            parametros = new Object[1];
            parametros[0] = classe;
            String nCodigo = (String) f.executaMetodo("control." + nomeTabela + "DAO", "deletaRegistro", parametros);
            //Busca JSON (grid) para atualizar a tela
            parametros = new Object[1];
            parametros[0] = classe;
            String partJSON = (String) f.executaMetodo("control." + nomeTabela + "DAO", "selecionaDados", parametros);
            return "{\"tela\":{" + nCodigo + "," + partJSON + "}}";
//        return "{\"dados\":[" + nCodigo + "," + partJSON + "]}";
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #032.\nOcorreu um erro na tentativa de deletar o registro!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public String getTelaConsulta(Object classe, Parametros params) {
        try {
            String nomeTabela = this.getClass().getSimpleName().substring(0, this.getClass().getSimpleName().length() - 10);
            Funcoes f = new Funcoes();
            Object[] parametros;
            //Busca o JSON para montar o combo de campos da tela de consulta
            parametros = new Object[0];
            String partJSON1 = (String) f.executaMetodo(classe, "obterCamposTelaConsulta", parametros);
            //Busca JSON (grid) para atualizar a tela
            parametros = new Object[2];
            parametros[0] = classe;
            parametros[1] = params.getParameter("condicao") != null ? params.getParameter("condicao") : " 1 <> 1";
            String partJSON2 = (String) f.executaMetodo("control." + nomeTabela + "DAO", "consultaDados", parametros);
            return "{\"consulta\":{" + partJSON1 + "," + partJSON2 + "}}";
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #033.\nOcorreu um erro na tentativa de obter a tela de consulta!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

    public String retornaDadosConsulta(Object classe, Parametros params) {
        try {
            String nomeTabela = this.getClass().getSimpleName().substring(0, this.getClass().getSimpleName().length() - 10);
            Funcoes f = new Funcoes();
            Object[] parametros;
            //Busca JSON (grid) para atualizar a tela
            parametros = new Object[2];
            parametros[0] = classe;
            parametros[1] = params.getParameter("condicao") != null ? params.getParameter("condicao") : " 1 <> 1";
            String partJSON1 = (String) f.executaMetodo("control." + nomeTabela + "DAO", "consultaDados", parametros);
            return "{\"consulta\":{" + partJSON1 + "}}";
        } catch (Exception ex) {
            Erro erro = new Erro().getInstance();
            if (!erro.ocorreuErro()) {
                String msgErro = "erro:Código de erro #034.\nOcorreu um erro na tentativa de retornar os dados da consulta!";
                if (false) {
                    msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                }
                erro.setErro(msgErro, ex.getMessage());
            }
            return null;
        }
    }

}
