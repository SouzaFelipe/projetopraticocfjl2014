
package br.com.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Felipe
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ParametroRel {
    //Atributos
    String descricao();                 //=> Label do campo
    String tipo();                      //=> Tipo de dados do campo
    
    String chave() default "N";         //=> Campo chave P/F/N
    String tabelaOrigem() default "";   //=> Tabela de origem FK
    String campoOrigem() default "";    //=> Campo de origem FK
    String campoRetorno() default "";   //=> Campo de retorno FK
    String labelCampoRetorno() default "";   //=> Lavel de campo FK
    
    String opcoesJSON() default "[]";   //=> Opções fixas para o campo ComboBox --> JSON
    
    String valor() default "";          //=> Valor padrão para o campo
    String required() default "N";      //=> Campo obrigatório
    int largura() default 95;           //=> Largura do campo
    int maxlength() default -1;         //=> Numero maximo de caracteres
    String readonly() default "N";      //=> Campos somente leitura
    int ordem() default -1;             //=> Ordem dos campos
    //Eventos
    String onfocus() default "";        //=> Evento de foco do campo
    String onkeydown() default "";      //=> Evento de keydown do campo
    String onkeypress() default "";     //=> Evento de keypress do campo
    String onblur() default "";         //=> Evento de onblur do campo
}
