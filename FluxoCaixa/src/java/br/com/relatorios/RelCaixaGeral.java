/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.relatorios;

import br.com.framework.Classe;
import br.com.framework.Erro;
import br.com.framework.IRelatorio;
import br.com.framework.ParametroRel;
import br.com.framework.Parametros;
import br.com.framework.Relatorio;
import java.sql.ResultSet;

/**
 *
 * @author Henrique
 */
public class RelCaixaGeral extends Relatorio implements IRelatorio {

    @Classe(titulo = "Relatório do Caixa")
    private String classe;
    @ParametroRel(descricao = "Informe o Caixa", tipo = "S", ordem = 1, largura = 190)
    private String descricao;

    @Override
    public String geraRelatorio(Parametros params) {
        StringBuilder retorno = new StringBuilder("");
        String acao = params.getParameter("acao");
        if (acao == null) {
            retorno.append(obterParametros(this.getClass(), params));
        } else {
            try {
                StringBuilder SQL = new StringBuilder("");
                StringBuilder SQLTemp = new StringBuilder("");
                SQLTemp.append(" SELECT T.CODCAIXA, T.DESCRICAO, T.SALDO, T.STATUS, T.DATACADASTRO, T.DATAFECHAMENTO")
                       .append(" FROM CAIXA T ")
                       .append(" WHERE UPPER(T.DESCRICAO) LIKE UPPER(:DESCRICAO) ")
                       .append(" ORDER BY T.DESCRICAO ");
                SQL.append(preparaSQL(this.getClass(), SQLTemp.toString(), params));
                ResultSet registros = executaSQL(SQL.toString());
                int qtdeRegistros = 0;
                iniciaRel();
                addCabecalhoTabela("<tr>"
                        + "<th></th><th>Código</th>"
                        + "<th>Descrição</th>"
                        + "<th>Saldo</th>"
                        + "<th>Status</th>"
                        + "<th>Data Abertura</th>"
                        + "<th>Data Fechamento</th></tr>");
                while (registros.next()) {
                    qtdeRegistros++;
                    String color;
                    String status = "";
                    if (registros.getString("STATUS").equals("A")) {
                        color = "green";
                        status = "# Ativo";
                    } else {
                        color = "red";
                        status = "@ Itivo";
                    }
                    addHTMLRel("<tr><td style=\"background:" + color + ";\">" + status + " </td><td class=\"centro borda negrito\">" + registros.getString("CODCAIXA") + "</td>"
                            + "<td class=\"borda\">" + registros.getString("DESCRICAO") + "</td>"
                            + "<td class=\"borda\">" + registros.getString("SALDO") + "</td>"
                            + "<td class=\"borda\">" + registros.getString("STATUS") + "</td>"
                            + "<td class=\"borda\">" + registros.getString("DATACADASTRO") + "</td>"
                            + "<td class=\"borda\">" + registros.getString("DATAFECHAMENTO") + "</td></tr>");
                }
                if (qtdeRegistros == 0) {
                    retornaErro();
                } else {
                    registros.getStatement().getConnection().close();
                }
                finalizaRel();
                retorno.append("\"pdf\":\"").append(geraPDF()).append("\"");
            } catch (Exception ex) {
                Erro erro = new Erro().getInstance();
                if (!erro.ocorreuErro()) {
                    String msgErro = "erro:Código de erro #70.RelHistorico.\nOcorreu um erro na tentativa de gerar o relatório!";
                    if (false) {
                        msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                    }
                    erro.setErro(msgErro, ex.getMessage());
                }

                return null;
            }
        }
        return "{\"relatorio\":{" + retorno.toString() + "}}";
    }
}
