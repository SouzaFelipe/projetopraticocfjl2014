package br.com.relatorios;

import br.com.framework.Classe;
import br.com.framework.Erro;
import br.com.framework.IRelatorio;
import br.com.framework.ParametroRel;
import br.com.framework.Parametros;
import br.com.framework.Relatorio;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

/**
 * @date 12.May.2014
 * @author Asllanne
 */
public class RelContas extends Relatorio implements IRelatorio {

    @Classe(titulo = "Relatório de Contas")
    private String classe;

    @ParametroRel(descricao = "Data inicial", tipo = "D", ordem = 1, largura = 150, required = "S")
    private String dtInicio;
    @ParametroRel(descricao = "Data final", tipo = "D", ordem = 2, largura = 150, required = "S")
    private String dtFim;

    @Override
    public String geraRelatorio(Parametros params) {
        StringBuilder retorno = new StringBuilder("");
        String acao = params.getParameter("acao");
        if (acao == null) { //Monta parâmetros do relatório
            retorno.append(obterParametros(this.getClass(), params));
        } else {
            try {
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd.MM.yyyy");
                SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");

                StringBuilder SQL = new StringBuilder("");
                StringBuilder SQLTemp = new StringBuilder("");
                SQLTemp.append("SELECT C.*, P.NOME, T.DESCRICAO")
                        .append(" FROM CONTA C")
                        .append(" INNER JOIN CLIFOR P ON P.CODCLIFOR = C.CODCLIFOR ")
                        .append(" INNER JOIN TIPOCONTA T ON T.CODTIPOCONTA = C.CODTIPOCONTA")
                        .append(" WHERE DATAVENCIMENTO BETWEEN  " + sdf1.parse(dtInicio) + " AND " + sdf1.parse(dtFim));
                SQL.append(preparaSQL(this.getClass(), SQLTemp.toString(), params));
                ResultSet registros = executaSQL(SQL.toString());
                int qtdeRegistros = 0;
                iniciaRel();
                addCabecalhoTabela("<tr style=\"font-size:13px;page:land;\">"
                        + "<th style=\"width:100px;\">Cód.</th>"
                        + "<th style=\"width:250px;\">Tipo Conta</th>"
                        + "<th style=\"width:50px;\">Favorecido</th>"
                        + "<th style=\"width:100px;\">Vencimento</th>"
                        + "<th style=\"width:100px;\">Valor</th>"
                        + "<th style=\"width:100px;\">Pagar/Raceber</th>"
                        + "<th style=\"width:100px;\">Data Baixa</th>"
                        + "<th style=\"width:100px;\">Valor baixa</th>"
                        + "</tr>");
                while (registros.next()) {
                    qtdeRegistros++;
                    addHTMLRel("<tr>"
                            + "<td class=\"centro borda negrito\">" + registros.getString("CODCONTA") + "</td>"
                            + "<td class=\"borda\">" + registros.getString("DESCRICAO") + "</td>"
                            + "<td class=\"borda\">" + registros.getString("NOME") + "</td>"
                            + "<td class=\"borda\">" + sdf2.format(registros.getString("DATAVENCIMENTO")) + "</td>"
                            + "<td class=\"borda\">" + registros.getString("VALOR") + "</td>"
                            + "<td class=\"borda\">" + registros.getString("PAGARRECEBER") + "</td>"
                            + "<td class=\"borda\">" + sdf2.format(registros.getString("DATABAIXA")) + "</td>"
                            + "<td class=\"borda\">" + registros.getString("VALORBAIXA") + "</td>"
                            + "</tr>");
                }
                if (qtdeRegistros == 0) {
                    retornaErro();
                } else {
                    registros.getStatement().getConnection().close();
                }
                finalizaRel();
                retorno.append("\"pdf\":\"").append(geraPDF()).append("\"");
            } catch (Exception ex) {
                Erro erro = new Erro().getInstance();
                if (!erro.ocorreuErro()) {
                    String msgErro = "erro:Código de erro #070.RelTipoConta.\nOcorreu um erro na tentativa de gerar o relatório!";
                    if (false) {
                        msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                    }
                    erro.setErro(msgErro, ex.getMessage());
                }
                return null;
            }
        }
        return "{\"relatorio\":{" + retorno.toString() + "}}";
    }
}
