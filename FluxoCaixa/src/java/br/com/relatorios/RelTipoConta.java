/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.relatorios;

import br.com.framework.Classe;
import br.com.framework.Erro;
import br.com.framework.IRelatorio;
import br.com.framework.ParametroRel;
import br.com.framework.Parametros;
import br.com.framework.Relatorio;
import java.sql.ResultSet;

/**
 *
 * @author felipesouza
 */
public class RelTipoConta extends Relatorio implements IRelatorio {

    @Classe(titulo = "Relatório de Conta por Tipo")
    private String classe;

    @ParametroRel(descricao = "Informe a descrição ou parte o nome da Conta", tipo = "S", ordem = 1, largura = 190)
    private String descricao;

    @Override
    public String geraRelatorio(Parametros params) {
        StringBuilder retorno = new StringBuilder("");
        String acao = params.getParameter("acao");
        if (acao == null) { //Monta parâmetros do relatório
            retorno.append(obterParametros(this.getClass(), params));
        } else {
            try {
                StringBuilder SQL = new StringBuilder("");
                StringBuilder SQLTemp = new StringBuilder("");
                SQLTemp.append("SELECT T.CODTIPOCONTA, T.DESCRICAO ")
                        .append(" FROM TIPOCONTA T")
                        .append(" WHERE UPPER(T.DESCRICAO) LIKE UPPER(:DESCRICAO)");
                SQL.append(preparaSQL(this.getClass(), SQLTemp.toString(), params));
                ResultSet registros = executaSQL(SQL.toString());
                int qtdeRegistros = 0;
                iniciaRel();
                imprimePaisagem();
                addCabecalhoTabela("<tr><th style=\"width:100px;\">Código</th><th>Tipo de Conta</th></tr>");
                while (registros.next()) {
                    qtdeRegistros++;
                    addHTMLRel("<tr>"
                            + "<td class=\"centro borda negrito\">" + registros.getString("CODTIPOCONTA") + "</td>"
                            + "<td class=\"borda\">" + registros.getString("DESCRICAO") + "</td>"
                            + "</tr>");
                }
                if (qtdeRegistros == 0) {
                    retornaErro();
                } else {
                    registros.getStatement().getConnection().close();
                }
                finalizaRel();
                retorno.append("\"pdf\":\"").append(geraPDF()).append("\"");
            } catch (Exception ex) {
                Erro erro = new Erro().getInstance();
                if (!erro.ocorreuErro()) {
                    String msgErro = "erro:Código de erro #070.RelTipoConta.\nOcorreu um erro na tentativa de gerar o relatório!";
                    if (false) {
                        msgErro += "\n\nInformações técnicas:\n" + ex.getMessage();
                    }
                    erro.setErro(msgErro, ex.getMessage());
                }
                return null;
            }
        }
        return "{\"relatorio\":{" + retorno.toString() + "}}";
    }
}
