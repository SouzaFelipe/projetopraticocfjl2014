package br.com.control;

import br.com.framework.DB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @date 05/05/2014
 * @author Asllanne
 */
public class TransferenciaEntreCaixaDAO {

    public String transfereValor(int caixaRetirar, int caixaAumentar, double valorRA) throws ClassNotFoundException, SQLException {
        Connection conn = DB.criaConexao();
        double saldoR = 0.0, saldoA = 0.0;
        String retorno;
        try {
            Statement stm = conn.createStatement();
            String sqlSaldoR = "SELECT SALDO FROM CAIXA WHERE CODCAIXA = " + caixaRetirar;
            ResultSet rsR = stm.executeQuery(sqlSaldoR);
            if (rsR.next()) {
                saldoR = rsR.getDouble("SALDO") - valorRA;
                String sqlSaldoA = "SELECT SALDO FROM CAIXA WHERE CODCAIXA = " + caixaAumentar;
                ResultSet rsA = stm.executeQuery(sqlSaldoA);
                if (rsA.next()) {
                    saldoA = rsA.getDouble("SALDO") + valorRA;

                    stm.executeUpdate("UPDATE CAIXA SET SALDO = '" + saldoR + "' WHERE CODCAIXA = " + caixaRetirar);
                    stm.executeUpdate("UPDATE CAIXA SET SALDO = '" + saldoA + "' WHERE CODCAIXA = " + caixaAumentar);

                    retorno = "{\"caixaAumentar\":" + caixaAumentar + ",\"caixaRetirar\":" + caixaRetirar + ",\"saldoA\":" + saldoA + ",\"saldoR\":" + saldoR + "}";
                } else {
                    retorno = "erro:Caixa inválido.";
                }
            } else {
                retorno = "erro:Caixa inválido.";
            }
        } finally {
            conn.close();
        }
        return retorno;
    }
}
