package br.com.control;

import br.com.framework.DAO;

/**
 * @date 10/03/2014
 * @author Asllanne
 */
public class TipoContaDAO extends DAO {

    public String selecionaDados(Object objClass) {
        return retornaDados(objClass, null);
    }

    public String salvaDados(Object objClass, Integer codigo) {
        return gravaDados(objClass, codigo);
    }

    public String deletaRegistro(Object objClass) {
        return deletaDados(objClass);
    }

    public String consultaDados(Object objClass, String condicao) {
        return retornaDados(objClass, condicao);
    }

}
