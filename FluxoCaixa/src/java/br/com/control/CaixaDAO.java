/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.control;

import br.com.framework.DAO;

/**
 *
 * @author usuario
 */
public class CaixaDAO extends DAO{
    
    
    public String selecionaDados(Object objClass) {
        return retornaDados(objClass, null);
    }

    public String salvaDados(Object objClass, Integer codigo) {
        return gravaDados(objClass, codigo);
    }

    public String deletaRegistro(Object objClass) {
        return deletaDados(objClass);
    }

    public String consultaDados(Object objClass, String condicao) {
        return retornaDados(objClass, condicao);
    }

}
