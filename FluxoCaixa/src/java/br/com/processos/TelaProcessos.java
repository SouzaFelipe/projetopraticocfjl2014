/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.processos;

import br.com.framework.Botao;
import br.com.framework.Campo;
import br.com.framework.Classe;
import br.com.framework.Processo;

/**
 *
 * @author felipesouza
 */
public class TelaProcessos extends Processo {
    
    @Classe(titulo = "Tela de Processos de Teste")
    private String titulo;
    
    @Campo(descricao = "Nome", tipo = "S", largura = 280, maxlength = 15, ordem = 1)
    private String nome;
    @Campo(descricao = "Bairro", tipo = "S", largura = 220, maxlength = 10, ordem = 2)
    private String bairro;
    @Campo(descricao = "CIdade", tipo = "S", largura = 100, maxlength = 10, ordem = 3)
    private String cidade;
    @Campo(descricao = "Estado", tipo = "S", largura = 150, maxlength = 2, ordem = 4)
    private String uf;
    
    @Botao(label = "OK", onclick = "metodoTeste(this);")
    private String btnOK;
    @Botao(label = "Cancelar", onclick = "fechaTelaProcesso(this);")
    private String btnCancelar;
}
