package br.com.processos;

import br.com.framework.Botao;
import br.com.framework.Campo;
import br.com.framework.Classe;
import br.com.framework.Processo;

public class BaixaConta extends Processo {
    
    
    @Classe(titulo = "Baixar Conta")
    private String titulo;
    
    @Campo(descricao = "Código da Conta", tipo = "I", readonly = "S", ordem = 1, largura = 105)
    private int codConta;
    
    @Campo(descricao = "Pagar/Receber", tipo = "C", ordem = 2, opcoesJSON = "[{\"valor\":\"P\", \"label\":\"Pagar\"},{\"valor\":\"R\", \"label\":\"Receber\"}]", required = "S")
    private String pagarReceber;

    @Campo(descricao = "Data da Baixa", tipo = "D", largura = 72, ordem = 3)
    private String dataBaixa;
    
    @Campo(descricao = "Vencimento", tipo = "D", largura = 72, ordem = 4)
    private String dataVencimento;

    @Campo(descricao = "Codigo Caixa", tipo = "I", chave = "F", largura = 105, ordem = 5, tabelaOrigem = "Caixa", campoOrigem = "CODCAIXA", campoRetorno = "DESCRICAO", labelCampoRetorno = "Caixa")
    private String codcaixa;

    @Campo(descricao = "Valor", tipo = "N", ordem = 6)
    private String valor;

    @Botao(label = "Baixar", onclick = "lancaConta(this);")
    private String btnOK;
    
    @Botao(label = "Cancelar", onclick = "fechaTelaProcesso(this);")
    private String btnCancelar;
    
}