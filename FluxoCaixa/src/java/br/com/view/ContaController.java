package br.com.view;

import br.com.framework.Parametros;
import br.com.framework.View;
import br.com.model.Conta;

/**
 *
 * @author Henrique
 */
public class ContaController extends View {

    private final Conta conta;

    public ContaController() {
        conta = new Conta();
    }

    public String obterTela(Parametros parametros) {
        return getTela(conta);
    }

    public String salvarDados(Parametros parametros) {
        return saveDados(conta, parametros);
    }

    public String deletarRegistro(Parametros parametros) {
        return deleteDados(conta, parametros);
    }

    public String obterTelaConsulta(Parametros parametros) {
        return getTelaConsulta(conta, parametros);
    }

    public String executaConsulta(Parametros parametros) {
        return retornaDadosConsulta(conta, parametros);
    }
}
