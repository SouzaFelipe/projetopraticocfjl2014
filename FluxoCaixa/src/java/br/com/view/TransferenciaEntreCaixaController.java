package br.com.view;

import br.com.control.TransferenciaEntreCaixaDAO;
import br.com.framework.Parametros;
import br.com.framework.ViewProcesso;
import br.com.processos.TransferenciaEntreCaixa;

/**
 *
 * @author Asllanne
 */
public class TransferenciaEntreCaixaController extends ViewProcesso {

    private final TransferenciaEntreCaixa transferenciaEntreCaixa;

    public TransferenciaEntreCaixaController() {
        transferenciaEntreCaixa = new TransferenciaEntreCaixa();
    }

    public String obterTela(Parametros parametros) {
        return getTela(transferenciaEntreCaixa, parametros);
    }

    public String transfereCaixa(Parametros parametros) {
        int caixaRetirar = Integer.parseInt(parametros.getParameter("CODCAIXA"));
        int caixaAumentar = Integer.parseInt(parametros.getParameter("CODCAIXAAUMENTAR"));
        double valorRA = Double.parseDouble(parametros.getParameter("VALORAUMENTARRETIRAR").replace(".", "").replace(",", "."));
        String retorno;
        try {
            TransferenciaEntreCaixaDAO tcDAO = new TransferenciaEntreCaixaDAO();
            retorno = tcDAO.transfereValor(caixaRetirar, caixaAumentar, valorRA);

            if (retorno.equals("false")) {
                retorno = "erro:Algum caixa informado não é válido.\nVerifique!";
            }
        } catch (Exception ex) {
            retorno = "erro:Destrua a torre negra!";
        }
        return retorno;
    }

}
