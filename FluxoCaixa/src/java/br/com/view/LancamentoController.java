package br.com.view;

import br.com.framework.Parametros;
import br.com.framework.View;
import br.com.model.Lancamento;

public class LancamentoController extends View {
    
        private final Lancamento lancamento;

    public LancamentoController() {
        lancamento = new Lancamento();
    }

    public String obterTela(Parametros parametros) {
        return getTela(lancamento);
    }

    public String salvarDados(Parametros parametros) {
        return saveDados(lancamento, parametros);
    }

    public String deletarRegistro(Parametros parametros) {
        return deleteDados(lancamento, parametros);
    }

    public String obterTelaConsulta(Parametros parametros) {
        return getTelaConsulta(lancamento, parametros);
    }

    public String executaConsulta(Parametros parametros) {
        return retornaDadosConsulta(lancamento, parametros);
    }
    
}
