/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.view;

import br.com.framework.Parametros;
import br.com.framework.View;
import br.com.model.Historico;

/**
 *
 * @author Henrique
 */
public class HistoricoController extends View{
    
    private final Historico historico;
    
    public HistoricoController() {
        historico = new Historico();
    }
    
    public String obterTela(Parametros parametros) {
        return getTela(historico);
    }
    
    public String salvarDados(Parametros parametros) {
        return saveDados(historico, parametros);
    }
    
    public String deletarRegistro(Parametros parametros) {
        return deleteDados(historico, parametros);
    }
    
    public String obterTelaConsulta(Parametros parametros) {
        return getTelaConsulta(historico, parametros);
    }
        
    public String executaConsulta(Parametros parametros) {
        return retornaDadosConsulta(historico, parametros);
    }
}
