package br.com.view;

import br.com.framework.Parametros;
import br.com.framework.ViewProcesso;
import br.com.processos.TelaProcessos;

/**
 *
 * @author felipesouza
 */
public class TelaProcessosController extends ViewProcesso {

    private final TelaProcessos telaProcessos;

    public TelaProcessosController() {
        telaProcessos = new TelaProcessos();
    }

    public String obterTela(Parametros parametros) {
        return getTela(telaProcessos, parametros);
    }

    public String metodoTeste(Parametros parametros) {
        String a = parametros.getParameter("NOME");
        String b = parametros.getParameter("BAIRRO");
        String c = parametros.getParameter("CIDADE");
        String d = parametros.getParameter("UF");
        return a + " | " + b + " | " + c + " | " + d;
    }
}
