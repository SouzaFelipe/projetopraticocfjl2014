package br.com.view;

import br.com.framework.Parametros;
import br.com.framework.View;
import br.com.model.TipoConta;

/**
 * @date 10/03/2014
 * @author Asllanne
 */
public class TipoContaController extends View{
    private final TipoConta tipoConta;

    public TipoContaController() {
        tipoConta = new TipoConta();
    }

    public String obterTela(Parametros parametros) {
        return getTela(tipoConta);
    }

    public String salvarDados(Parametros parametros) {
        return saveDados(tipoConta, parametros);
    }

    public String deletarRegistro(Parametros parametros) {
        return deleteDados(tipoConta, parametros);
    }

    public String obterTelaConsulta(Parametros parametros) {
        return getTelaConsulta(tipoConta, parametros);
    }

    public String executaConsulta(Parametros parametros) {
        return retornaDadosConsulta(tipoConta, parametros);
    }
}
