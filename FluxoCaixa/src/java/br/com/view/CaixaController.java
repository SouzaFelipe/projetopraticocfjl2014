package br.com.view;

import br.com.framework.Parametros;
import br.com.framework.View;
import br.com.model.Caixa;

public class CaixaController extends View{

    private final Caixa caixa;

    public CaixaController() {
        caixa = new Caixa();
    }

    public String obterTela(Parametros parametros) {
        return getTela(caixa);
    }

    public String salvarDados(Parametros parametros) {
        return saveDados(caixa, parametros);
    }

    public String deletarRegistro(Parametros parametros) {
        return deleteDados(caixa, parametros);
    }

    public String obterTelaConsulta(Parametros parametros) {
        return getTelaConsulta(caixa, parametros);
    }

    public String executaConsulta(Parametros parametros) {
        return retornaDadosConsulta(caixa, parametros);
    }
}
