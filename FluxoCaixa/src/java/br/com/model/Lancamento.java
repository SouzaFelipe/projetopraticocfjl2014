package br.com.model;

import br.com.framework.Campo;
import br.com.framework.Tabela;
import java.util.Date;

public class Lancamento extends Tabela {
    
    @Campo(descricao = "Código", tipo = "I", chave = "P", ordem = 1, readonly = "S", largura = 40)
    private int codlancamento;
    
    @Campo(descricao = "Cód. Caixa", tipo = "I", chave = "F", ordem = 2, tabelaOrigem = "Caixa", campoOrigem = "CODCAIXA", campoRetorno = "DESCRICAO", labelCampoRetorno = "Caixa", largura = 90, required = "S")
    private int codcaixa;
    
    @Campo(descricao = "Cód. Histórico", tipo = "I", chave = "F", ordem = 3, tabelaOrigem = "Historico", campoOrigem = "CODHISTORICO", campoRetorno = "DESCRICAO", labelCampoRetorno = "Histórico", largura = 90, required = "S")
    private int codhistorico;
    
    @Campo(descricao = "Movimentação", tipo = "D",largura = 72, ordem = 4, required = "S")
    private Date data;
    
    @Campo(descricao = "Referencia", tipo = "S", ordem = 5)
    private String documento;
    
    @Campo(descricao = "Valor", tipo = "N", ordem = 6, required = "S")
    private double valor;

    @Campo(descricao = "Complemento Histórico", tipo = "S",largura = 200, ordem = 8)
    private String complementohistorico; 

    @Campo(descricao = "Fechamento", tipo = "C", ordem = 7, largura = 70, opcoesJSON = "[{\"valor\":\"C\", \"label\":\"Crédito\"},{\"valor\":\"D\", \"label\":\"Débito\"}]", required = "S")
    private String debitocredito; 
    
}
