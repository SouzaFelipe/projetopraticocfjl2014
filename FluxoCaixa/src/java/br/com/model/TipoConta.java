package br.com.model;

import br.com.framework.Campo;
import br.com.framework.Opcoes;
import br.com.framework.Tabela;

/**
 * @date 10/03/2014
 * @author Asllanne
 */
public class TipoConta extends Tabela {

    @Campo(descricao = "Código", tipo = "I", chave = "P", readonly = "S", largura = 40, ordem = 1)
    private int codTipoConta;
    @Campo(descricao = "Descrição da conta", tipo = "S", largura = 250, ordem = 2, required = "S")
    private String descricao;
    
    @Opcoes(descricao = "Relatório de Tipos de Conta", onclick = "executaRelatorio('RelTipoConta');//", ordem = 1)
    private final String relTipoConta = "";
}
