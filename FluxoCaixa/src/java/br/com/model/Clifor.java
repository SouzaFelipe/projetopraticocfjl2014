package br.com.model;

import br.com.framework.Campo;
import br.com.framework.Opcoes;
import br.com.framework.Tabela;
import java.util.Date;

/*
 * @date 07/03/2014
 * @author Asllanne
 */
public class Clifor extends Tabela {

    @Campo(descricao = "Código", tipo = "I", chave = "P", readonly = "S", ordem = 1, largura = 40, required = "S")
    private int codclifor;
    @Campo(descricao = "Nome Completo", tipo = "S", maxlength = 70, ordem = 2, largura = 280, required = "S")
    private String nome;
    @Campo(descricao = "Nome Fantasia/Apelido", tipo = "S", maxlength = 70, ordem = 3, largura = 280)
    private String nomeFantasia;
    @Campo(descricao = "Data Nascimento/Fundação", tipo = "D", ordem = 4, largura = 72)
    private Date dtNasFun;
    @Campo(descricao = "CPF/ CNPJ", tipo = "CPFCNPJ", maxlength = 18, ordem = 5, largura = 120, required = "S")
    private String cpfCnpj;
    @Campo(descricao = "Telefone", tipo = "F", maxlength = 15, ordem = 6, largura = 94, grid = "S")
    private String telefone;
    @Campo(descricao = "E-mail", tipo = "S", maxlength = 70, ordem = 7, largura = 400)
    private String email;
    @Campo(descricao = "Ativo/Inativo", tipo = "C", ordem = 8, opcoesJSON = "[{\"valor\":\"A\", \"label\":\"Ativo\"},{\"valor\":\"I\", \"label\":\"Inativo\"}]", required = "S")
    private String status;
    @Campo(descricao = "Tipo (C_Cliente/F_Fornecedor/T_Transportador)", tipo = "S", maxlength = 3, largura = 30, ordem = 9, required = "S")
    private String tipo;
    @Campo(descricao = "Número", tipo = "I", largura = 50, ordem = 11)
    private int numero;
    @Campo(descricao = "Endereço", tipo = "S", maxlength = 70, largura = 200, ordem = 10, required = "S")
    private String logradouro;
    @Campo(descricao = "Complemento", tipo = "S", maxlength = 20, largura = 125, ordem = 12)
    private String complemento;
    @Campo(descricao = "Bairro", tipo = "S", maxlength = 50, largura = 195, ordem = 13, required = "S")
    private String bairro;
    @Campo(descricao = "Cidade", tipo = "S", maxlength = 70, largura = 225, ordem = 14, required = "S")
    private String cidade;
    @Campo(descricao = "UF", tipo = "S", maxlength = 2, largura = 22, ordem = 15, required = "S")
    private String uf;

    @Opcoes(descricao = "Relatório de Pessoas", onclick = "executaRelatorio('RelClifortrans');//", ordem = 1)
    private final String relClifortran = "";
    @Opcoes(descricao = "Relatório de Ativos", onclick = "executaRelatorio('RelClifortransAT');//", ordem = 2)
    private final String relClifortranAT = "";
    @Opcoes(descricao = "Relatório de Clientes", onclick = "executaRelatorio('RelClientes');//", ordem = 3)
    private final String relClientes = "";
    @Opcoes(descricao = "Relatório de Fornecedores", onclick = "executaRelatorio('RelFornecedores');//", ordem = 4)
    private final String relFornecedores = "";
    @Opcoes(descricao = "Relatório de Transportadores", onclick = "executaRelatorio('RelTransportadores');//", ordem = 5)
    private final String relTransportadores = "";
}
