package br.com.model;

import br.com.framework.Campo;
import br.com.framework.Opcoes;
import br.com.framework.Tabela;

public class Historico extends Tabela{
    
    @Campo(descricao = "Codigo", tipo = "I", chave = "P", readonly = "S", ordem = 1,largura = 40)
    private int codhistorico;
    @Campo(descricao = "Descrição", tipo = "S", ordem = 2, largura = 280, required = "S")
    private String descricao;
    
    @Opcoes(descricao = "Relatório de Historico", onclick = "executaRelatorio('RelHistorico');//", ordem = 1)
    private final String relHistorico = "";
}
