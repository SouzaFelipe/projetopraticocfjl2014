package br.com.model;

import br.com.framework.Campo;
import br.com.framework.Opcoes;
import br.com.framework.Tabela;
import java.util.Date;

/**
 *
 * @author Henrique
 */
public class Conta extends Tabela {

    @Campo(descricao = "Código", tipo = "I", chave = "P", readonly = "S", ordem = 1, largura = 40)
    private int codConta;
    @Campo(descricao = "Código", tipo = "I", chave = "F", ordem = 2, largura = 40, tabelaOrigem = "TipoConta", campoOrigem = "CODTIPOCONTA", campoRetorno = "DESCRICAO", labelCampoRetorno = "Tipo de Conta", required = "S")
    private int codTipoConta;
    @Campo(descricao = "Vencimento", tipo = "D", ordem = 3, largura = 72, required = "S")
    private Date dataVencimento;
    @Campo(descricao = "Valor", tipo = "N", ordem = 4, required = "S")
    private double valor;
    @Campo(descricao = "Pagar/Receber", tipo = "C", ordem = 5, opcoesJSON = "[{\"valor\":\"P\", \"label\":\"Pagar\"},{\"valor\":\"R\", \"label\":\"Receber\"}]", required = "S")
    private String pagarReceber;
    @Campo(descricao = "Data da Baixa", tipo = "D", ordem = 6, largura = 72)
    private Date dataBaixa;
    @Campo(descricao = "Valor da Baixa", tipo = "N", ordem = 7)
    private double valorbaixa;
    @Campo(descricao = "Cliente/Fornecedor", tipo = "I", chave = "F", ordem = 9, tabelaOrigem = "Clifor", campoOrigem = "CODCLIFOR", campoRetorno = "NOME", labelCampoRetorno = "Cliente/Fornecedor/Transportador")
    private int codCliFor;

    ////////////////////////////////////////////////////////////////////////////
    @Opcoes(descricao = "Baixar Conta", onclick = "ContaProcessos", ordem = 1)
    private final String baixaConta = "";
    @Opcoes(descricao = "Relatório de Contas", onclick = "executaRelatorio('RelContas');//", ordem = 2)
    private final String relContas = "";
}
