package br.com.model;

import br.com.framework.Campo;
import br.com.framework.Opcoes;
import br.com.framework.Tabela;
import java.util.Date;

public class Caixa extends Tabela {

    @Campo(descricao = "Código", tipo = "I", chave = "P", ordem = 1, readonly = "S", largura = 40)
    private int codcaixa;
    @Campo(descricao = "Descrição do caixa", tipo = "S", ordem = 2, largura = 145, required = "S")
    private String descricao;
    @Campo(descricao = "Valor anterior", tipo = "N", ordem = 3, required = "S")
    private double saldo;
    @Campo(descricao = "Ativo/Inativo", tipo = "C", ordem = 4, opcoesJSON = "[{\"valor\":\"A\", \"label\":\"Ativo\"},{\"valor\":\"I\", \"label\":\"Inativo\"}]", required = "S")
    private String status;
    @Campo(descricao = "Abertura", tipo = "D", ordem = 5, largura = 72, required = "S")
    private Date datacadastro;
    @Campo(descricao = "Fechamento", tipo = "D", ordem = 6, largura = 72)
    private Date datafechamento;
    @Opcoes(descricao = "Transferência entre Caixas", onclick = "tranferenciaCaixa", ordem = 2)
    private final String transferencia = "";
    @Opcoes(descricao = "Relatorio de Status", onclick = "executaRelatorio('RelCaixaStatus');//", ordem = 3)
    private final String ralatorioStatus = "";
    @Opcoes(descricao = "Relatorio Geral de Caixas", onclick = "executaRelatorio('RelCaixaGeral');//", ordem = 4)
    private final String ralatorioGeral = "";
}
