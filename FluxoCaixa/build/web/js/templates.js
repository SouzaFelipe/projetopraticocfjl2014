
var template = {
    "menu": "<div>" +
            "   <nav class=\"nav-menu\">" +
            "       <ul>" +
            "           <li><a onclick=\"carregaHome()\">Home</a></li>" +
            "           <li><a cls=\"CliforController\" mtd=\"obterTela\" callback=\"carregaCadastro\" onclick=\"executaMenu(this);\">Pessoa</a></li>" +
            "           <li><a cls=\"TipoContaController\" mtd=\"obterTela\" callback=\"carregaCadastro\" onclick=\"executaMenu(this);\">Tipo de Conta</a></li>" +
            "           <li><a cls=\"ContaController\" mtd=\"obterTela\" callback=\"carregaCadastro\" onclick=\"executaMenu(this);\">Conta</a></li>" +
            "           <li><a cls=\"HistoricoController\" mtd=\"obterTela\" callback=\"carregaCadastro\" onclick=\"executaMenu(this);\">Historico</a></li>" +
            "           <li><a cls=\"CaixaController\" mtd=\"obterTela\" callback=\"carregaCadastro\" onclick=\"executaMenu(this);\">Caixa</a></li>" +
            "           <li><a cls=\"LancamentoController\" mtd=\"obterTela\" callback=\"carregaCadastro\" onclick=\"executaMenu(this);\">Lançamento</a></li>" +
            "       </ul>" +
            "   </nav>" +
            "   <div class=\"div-conteudo\">" +
            "       {{#corpo}}" +
            "   </div>" +
            "</div>",
    "cadastro": "<div>" +
            "   <div class=\"barra-botoes\">" +
            "       <button id=\"btnNovo\" class=\"btn\" onclick=\"btnNovo(this);\"><img src=\"img/novo.png\" alt=\"\"> Novo</button>" +
            "       <button id=\"btnEditar\" class=\"btn\" onclick=\"btnEditar(this);\"><img src=\"img/editar.png\" alt=\"\"> Editar</button>" +
            "       <button id=\"btnSalvar\" disabled class=\"btn-desabilitado\" onclick=\"btnSalvar(this);\"><img src=\"img/salvar.png\" alt=\"\"> Salvar</button>" +
            "       <button id=\"btnCancelar\" disabled class=\"btn-desabilitado\" onclick=\"btnCancelar(this);\"><img src=\"img/cancelar.png\" alt=\"\"> Cancelar</button>" +
            "       <button id=\"btnDeletar\" class=\"btn\" onclick=\"btnDeletar(this);\"><img src=\"img/deletar.png\" alt=\"\"> Deletar</button>" +
            "       <button id=\"btnConsultar\" class=\"btn\" onclick=\"btnConsultar(this);\" nometabela=\"{{#labeltabela}}\"><img src=\"img/consultar.png\" alt=\"\"> Consultar</button>" +
            "       {{#opcoes}}" +
            "       <div style=\"clear:both;\"></div>" +
            "   </div>" +
            "   <form id=\"formCampos\" tabela=\"{{#idtabela}}\" style=\"padding:3px 0px 4px 5px;height:200px;overflow-y:auto;border:1px solid gray;margin:6px 0px 6px 0px;\">{{#campos}}</form>" +
            "   <div style=\"height:209px;overflow-y:scroll;overflow-x:auto;border:1px solid gray;position:relative;\">" +
            "       {{#tabela}}" +
            "       <div id=\"bloqueiaGrid\" style=\"display:none;position:absolute;top:0px;height:100%;width:100%;opacity:.4;background:#F0F0F0;\"></div>" +
            "   </div>" +
            "</div>",
    "consulta": "<div class=\"div-bloqueio\"></div>" +
            "<div class=\"div-dialog\" role=\"dialog\">" +
            "   <div class=\"div-dialog-cabecalho\">" +
            "       <label class=\"div-dialog-cabecalho-titulo\">Consulta de {{#tabela}} </label>" +
            "       <img src=\"img/fechar.png\" alt=\"Fechar\" title=\"Fechar\" class=\"div-dialog-cabecalho-fechar\" onclick=\"fechaTela(this);\" />" +
            "   </div>" +
            "   <div class=\"div-dialog-conteudo\">" +
            "       <div class=\"div-dados-consulta\">" +
            "           <div class=\"div-float-left\">" +
            "               <label>Campo</label><br />" +
            "               <select id=\"campo-consulta\" class=\"select-campos-consulta\">{{#campos}}</select>" +
            "           </div>" +
            "           <div class=\"div-float-left\">" +
            "               <label>Operação</label><br />" +
            "               <select id=\"operacao-consulta\">" +
            "                   <option>&lt;</option>" +
            "                   <option>&lt;=</option>" +
            "                   <option selected>=</option>" +
            "                   <option>&lt;&gt;</option>" +
            "                   <option>&gt;=</option>" +
            "                   <option>&gt;</option>" +
            "                   <option>Iniciar</option>" +
            "                   <option>Contém</option>" +
            "               </select>" +
            "           </div>" +
            "           <div class=\"div-float-left\">" +
            "               <label>Valor</label><br />" +
            "               <input type=\"text\" id=\"valor-consulta\" placeholder=\"Digita o valor aqui...\" class=\"campo-consulta\" />" +
            "               <button id=\"btnSalvar\" class=\"btn btn-consulta\" onclick=\"btnExecutaConsulta(this);\">" +
            "                   <img src=\"img/consultar.png\" alt=\"\" />" +
            "               </button>" +
            "               <div style=\"clear:both;\"></div>" +
            "           </div>" +
            "           <div class=\"div-clear-both\"></div>" +
            "       </div>" +
            "       <div class=\"div-grid-consulta\">{{#grid}}</div>" +
            "   </div>" +
            "</div>",
    "processos": "<div class=\"div-bloqueio\"></div>"+
            "<div class=\"div-dialog\" role=\"dialog\">"+
            "   <div class=\"div-dialog-cabecalho\">"+
            "       <label class=\"div-dialog-cabecalho-titulo\">{{#tituloprocesso}}</label>"+
            "       <img src=\"img/fechar.png\" alt=\"Fechar\" title=\"Fechar\" class=\"div-dialog-cabecalho-fechar\" onclick=\"fechaTelaProcesso(this);\">"+
            "   </div>"+
            "   <div class=\"div-dialog-conteudo\">"+
            "       <div class=\"div-dados-processos\">{{#formprocesso}}</div>"+
            "   </div>"+
            "</div>",
    "consultaFK": "<div class=\"div-bloqueio\"></div>" +
            "<div class=\"div-dialog\" role=\"dialog\">" +
            "   <div class=\"div-dialog-cabecalho\">" +
            "       <label class=\"div-dialog-cabecalho-titulo\">Consulta de {{#tabelaFK}} </label>" +
            "       <img src=\"img/fechar.png\" alt=\"Fechar\" title=\"Fechar\" class=\"div-dialog-cabecalho-fechar\" onclick=\"fechaTelaProcesso(this);\" />" +
            "   </div>" +
            "   <div class=\"div-dialog-conteudo\">" +
            "       <div class=\"div-dados-consulta\">" +
            "           <div class=\"div-float-left\">" +
            "               <label>Campo</label><br />" +
            "               <select id=\"campo-consulta\" class=\"select-campos-consulta\">{{#campos}}</select>" +
            "           </div>" +
            "           <div class=\"div-float-left\">" +
            "               <label>Operação</label><br />" +
            "               <select id=\"operacao-consulta\">" +
            "                   <option>&lt;</option>" +
            "                   <option>&lt;=</option>" +
            "                   <option selected>=</option>" +
            "                   <option>&lt;&gt;</option>" +
            "                   <option>&gt;=</option>" +
            "                   <option>&gt;</option>" +
            "                   <option>Iniciar</option>" +
            "                   <option>Contém</option>" +
            "               </select>" +
            "           </div>" +
            "           <div class=\"div-float-left\">" +
            "               <label>Valor</label><br />" +
            "               <input type=\"text\" id=\"valor-consulta\" placeholder=\"Digita o valor aqui...\" class=\"campo-consulta\" />" +
            "               <button id=\"btnSalvar\" class=\"btn btn-consulta\" onclick=\"btnExecutaConsulta(this, true);\">" +
            "                   <img src=\"img/consultar.png\" alt=\"\" />" +
            "               </button>" +
            "               <div style=\"clear:both;\"></div>" +
            "           </div>" +
            "           <div class=\"div-clear-both\"></div>" +
            "       </div>" +
            "       <div class=\"div-grid-consulta\">{{#grid}}</div>" +
            "   </div>" +
            "</div>"
};
